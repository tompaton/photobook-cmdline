from calendar import Calendar, day_name, month_name
from collections.abc import Iterable
from pathlib import Path

from book_index import BookCalendar, IndexBuilder
from book_layout.book_pdf import PhotoDetails

from photobook.book_util import PageDict, PhotoDict


def get_cal_index(builder: IndexBuilder) -> Iterable[PageDict]:
    cal_slots = 3
    cals = builder.cal_indexes
    while cals:
        page_cals, cals = cals[:cal_slots], cals[cal_slots:]
        result = []
        for cal in page_cals:
            table = cal.index_table_data(str)
            title = table.pop(0)[0]
            data: PhotoDict = {"caption": title, "cal": {}}
            for row in table:
                if not isinstance(row[0], str):
                    raise TypeError(row[0])
                data["cal"][int(row[0])] = f"{row[1]} {row[2]}".strip()
            result.append(data)

        yield {"page": result}


def get_cals(photos: list[PhotoDict]) -> list[BookCalendar]:
    dates: set[tuple[int, int]] = set()
    for photo in photos:
        year_month = _get_date(photo)[0]
        if year_month:
            dates.add(year_month)
    return [_get_book_calendar(*year_month) for year_month in sorted(dates)]


def _get_book_calendar(year: int, month: int) -> BookCalendar:
    c = Calendar()

    return BookCalendar(
        year,
        month,
        month_name[month],
        [
            (day, day_name[day_num])
            for week in c.monthdayscalendar(year, month)
            for day_num, day in enumerate(week)
            if day
        ],
    )


def _get_date(photo: PhotoDict) -> tuple[tuple[int, int] | None, int, str]:
    filename = photo.get("photo", "BLANK")
    if filename == "BLANK":
        return None, 0, ""

    details = PhotoDetails.load_photo(Path(filename))

    if details.exif_datetime:
        y, m = details.exif_datetime.split(" ")[0].split(":")[:2]
        year_month = int(y), int(m)

        day = int(details.exif_datetime.split(" ")[0].split(":")[2])

        hour = details.exif_datetime.split(" ")[1]

        return year_month, day, hour

    return None, 0, ""
