#!/usr/bin/env python
# ruff: noqa: T201

import time
from collections.abc import Callable
from pathlib import Path

import pydantic
import rich_click as click
import ruyaml
from book_layout.book_pdf import RenderMode

from photobook import operations, reference, terminal_ui
from photobook.reconciler_list import make_reconciler_list

console = terminal_ui.create_console()

click.rich_click.COMMAND_GROUPS = {
    "photobook": [
        {
            "name": "Primary commands",
            "commands": ["make"],
        },
        {
            "name": "Other commands",
            "commands": [
                "generate",
                "annotate",
                "render",
                "map-preview",
                "reference",
                "config",
            ],
        },
    ],
}


@click.group("photobook")
def cli() -> None:  # pragma: no cover
    """Photobook creation tool"""


@cli.group("generate")
def generate_group() -> None:  # pragma: no cover
    """
    Commands for generating elements of the book
    """


@cli.group("reference")
def reference_group() -> None:  # pragma: no cover
    """
    Reference commands for generating self-documentation
    """


@cli.group("config")
def config_group() -> None:  # pragma: no cover
    """
    Configuration commands
    """


@config_group.command()
def show() -> None:
    """
    Show photobook configuration
    """
    config = operations.Configuration.load()

    print("Photobook configuration:")
    print(f" {config.map_path=}")
    print(f" {config.font_path=}")
    print(f" {config.MAPBOX_KEY=}")


@cli.command()
@click.argument(
    "book_file",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    nargs=-1,
    required=True,
)
@click.option(
    "--out",
    "-o",
    type=click.Path(writable=True, dir_okay=False, path_type=Path),
    help="defaults to BOOK_FILE with .pdf suffix",
)
@click.option("--no-cover", is_flag=True, help="don't render cover pages")
@click.option("--no-pages", is_flag=True, help="don't render inside pages")
@click.option(
    "--no-annotate",
    is_flag=True,
    help="don't annotate the input yaml with page numbers etc.",
)
@click.option("--debug", is_flag=True, help="render bounding boxes etc. for debugging")
@click.option("--title", help="override book title")
@click.option("--spine", help="override book spine")
@click.option(
    "--launch",
    is_flag=True,
    help="open the generated pdf in viewer",
)
@click.option("--final", is_flag=True, help="render for final output")
# TODO: support rendering only a selected page for quick editing
def render(
    book_file: list[Path],
    *,
    out: Path | None = None,
    no_cover: bool = False,
    no_pages: bool = False,
    no_annotate: bool = False,
    debug: bool = False,
    final: bool = False,
    title: str | None = None,
    spine: str | None = None,
    launch: bool = False,
) -> None:
    """
    Render photobook from YAML description

    BOOK_FILE is the input filename, if multiple files are provided the books
    will be concatenated into a single pdf.
    """
    operations.configure()

    render_mode = RenderMode(debug=debug, final=final)
    render_mode.cover = not no_cover
    render_mode.pages = not no_pages
    render_mode.annotate = not no_annotate

    operations.do_render(
        console,
        book_file,
        render_mode,
        out=out,
        title=title,
        spine=spine,
        launch=launch,
    )


@generate_group.command()
@click.argument(
    "input_yaml",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    nargs=1,
    required=True,
)
def thumb_index(input_yaml: Path) -> None:
    """
    Generate photobook thumbnail index

    Generate photobook thumbnail index from YAML description
    and print to stdout.

    INPUT_YAML is the thumbnail index input filename.
    """
    operations.do_thumb_index(input_yaml)


@generate_group.command("book")
@click.argument(
    "photo_path",
    type=click.Path(file_okay=False, exists=True, path_type=Path),
    required=True,
)
@click.argument(
    "book_file",
    type=click.Path(writable=True, dir_okay=False, path_type=Path),
    required=True,
)
def generate_book(photo_path: Path, book_file: Path) -> None:
    """
    Generate book yaml

    Generate book yaml for all the photos in the given directory and write to
    BOOK_FILE.
    """

    status = console.status("[bold green] Generating...")
    operations.do_generate_book(console, status, photo_path, book_file)
    status.stop()


@cli.command("annotate")
@click.argument(
    "book_file",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    nargs=-1,
    required=True,
)
@click.option(
    "--maps",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    help=("Annotate h3indexes by looking up photo locations in yaml database of maps and locations"),
)
@click.option(
    "--dates",
    is_flag=True,
    help="Annotate with datetimes from photo EXIF data",
)
def annotate(book_file: list[Path], *, maps: Path | None = None, dates: bool = False) -> None:
    """
    Annotate book with details from photos

    Annotate BOOK_FILE with datetimes and/or h3indexes by looking up photo
    locations.
    """
    if not maps and not dates:
        msg = "You must use either --maps or --dates"
        raise click.UsageError(msg)

    operations.do_annotate(console, book_file, maps, dates=dates)


@generate_group.command("index")
@click.argument(
    "book_file",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    nargs=-1,
    required=True,
)
@click.option(
    "--maps",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    required=True,
    help="yaml database of maps and locations",
)
@click.option(
    "--out",
    "-o",
    type=click.Path(writable=True, dir_okay=False, path_type=Path),
    required=True,
    help="index output file",
)
@click.option("--insert-blanks", is_flag=True, help="Insert blank pages before index")
def generate_index(book_file: list[Path], maps: Path, out: Path, *, insert_blanks: bool = False) -> None:
    """
    Generate photobook map/calendar index

    Generate photobook map index from YAML descriptions of books
    and write to an index file.

    BOOK_FILE is the input filename, if multiple files are provided the books
    will be concatenated into a single volume.
    """
    config = operations.configure()
    operations.do_generate_index(
        console,
        book_file,
        maps,
        out,
        insert_blanks=insert_blanks,
        config=config,
    )


@cli.command()
@click.option(
    "--maps",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    required=True,
    help="yaml database of maps and locations",
)
@click.argument(
    "map_html",
    type=click.Path(writable=True, dir_okay=False, path_type=Path),
    required=True,
)
@click.option(
    "--launch",
    is_flag=True,
    help="open the map html preview in browser",
)
def map_preview(maps: Path, map_html: Path, *, launch: bool = False) -> None:
    """
    Generate map html preview

    Generate map html preview and write to MAP_HTML.
    """
    config = operations.configure()
    operations.do_map_preview(console, maps, map_html, launch=launch, config=config)


@cli.command()
@click.option(
    "--maps",
    type=click.Path(dir_okay=False, path_type=Path),
    help="yaml database of maps and locations",
)
@click.argument(
    "photo_path",
    type=click.Path(file_okay=True, path_type=Path),
    nargs=-1,
    required=True,
)
@click.option(
    "--final",
    is_flag=True,
    help="render pages and cover as separate files for final output",
)
@click.option("--debug", is_flag=True, help="render bounding boxes etc. for debugging")
@click.option(
    "--visualize",
    is_flag=True,
    help=(
        "Output a graphviz (.dot) graph visualizing the dependencies between the "
        "input and output files instead of executing"
    ),
)
@click.option(
    "--launch",
    is_flag=True,
    help="open the generated files in browser/pdf viewer",
)
@click.option(
    "--no-meta",
    is_flag=True,
    help="Don't add meta/warnings to input yml file",
)
def make(
    maps: Path | None,
    photo_path: list[Path],
    *,
    final: bool = False,
    debug: bool = False,
    visualize: bool = False,
    launch: bool = False,
    no_meta: bool = False,
    keep_looping: Callable[..., bool] = lambda: True,
) -> None:
    """
    Generate and render book

    Check for changes and generate/render book.
    """
    config = operations.configure()

    reconciler_list = list(
        make_reconciler_list(
            maps,
            photo_path,
            final=final,
            debug=debug and not visualize,
            launch=launch,
            no_meta=no_meta,
            config=config,
        )
    )

    if visualize:
        print(reference.render_reconciler_graph(reconciler_list))

    else:
        terminal_ui.title_panel(console, reconciler_list)

        # can't do this in the click.argument as we want to be able to visualize
        # files and paths that don't exist...
        if maps is not None:
            check_exists(maps, "--maps", "File")

        for f in photo_path:
            check_exists(f, "PHOTO_PATH...", "Directory")

        status = console.status("[bold green] Updating...")

        while keep_looping():  # TODO: this could be a --loop/--no-loop option
            for reconciler in reconciler_list:
                if reconciler.can_update():
                    status.start()
                    try:
                        reconciler.update(console, status)
                    except (
                        ruyaml.parser.ParserError,
                        ruyaml.scanner.ScannerError,
                        pydantic.ValidationError,
                    ) as e:
                        status.stop()
                        if not terminal_ui.retry_error(console, e):
                            return
                    break
            else:
                status.stop()
            time.sleep(0.2)


def check_exists(filename: Path, hint: str, obj_type: str = "Path") -> None:
    if not filename.exists():
        raise click.BadParameter(param_hint=hint, message=f"{obj_type} '{filename}' does not exist.")


@reference_group.command("aspects")
@click.option("--table", is_flag=True, help="output markdown table of aspect ratios to stdout")
@click.option("--svg", is_flag=True, help="output svg diagram of aspect ratios to stdout")
def reference_aspects(*, table: bool = False, svg: bool = False) -> None:  # pragma: no cover
    """Output available photo aspect ratios."""
    aspect_table, aspect_svg = reference.generate_reference_aspects()

    # tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook
    # reference aspects --table | clipboard

    if table:
        print(reference.md_aspect_table(aspect_table))

    # tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook
    # reference aspects --svg > docs/images/reference-aspect.svg

    if svg:
        print(aspect_svg)


@reference_group.command("layouts")
@click.option("--table", is_flag=True, help="output markdown table of page layouts to stdout")
@click.option(
    "--svg",
    is_flag=True,
    help="output svg diagrams of page layouts to docs/images/reference-layout-xxx.svg",
)
def reference_layouts(*, table: bool = False, svg: bool = False) -> None:  # pragma: no cover
    """Output available page layouts."""
    layout_table = []
    for layout_table_, svg_name, layout_svg in reference.generate_reference_layouts():
        layout_table.append(layout_table_)
        if svg:
            Path(f"./docs/{svg_name}").write_text(layout_svg)

    # tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook
    # reference layouts --svg --table | clipboard

    if table:
        print(reference.md_layout_table(layout_table))


@reference_group.command("cli")
def reference_cli() -> None:  # pragma: no cover
    """Generate reference documentation for commands and options."""

    reference.print_reference_cli(cli)


@reference_group.command("schema")
def reference_schema() -> None:  # pragma: no cover
    """Generate reference documentation for YAML schemas."""

    reference.print_reference_schema()
