from collections import defaultdict
from collections.abc import Iterable, Iterator
from pathlib import Path

from book_index import MapImage
from book_layout.book_schema import MapsModel
from h3 import h3


def print_map_preview_html(
    locations: MapsModel,
    map_images: list[MapImage],
    map_width: float,
    map_height: float,
) -> Iterator[str]:
    yield MAP_HTML_PRE

    # warning if different locations have the same hex
    # TODO: should pick up nested hexes too...
    hexes = defaultdict(list)
    for location in locations.locations:
        for loc_hex in location.hexes:
            h3index, resolution = loc_hex.split(",")
            hex1 = h3.h3_to_parent(h3index, int(resolution))
            hexes[hex1].append((location.location, h3index, resolution))
    yield "<h3>Issues</h3><ul>"
    bad_hexes: list[str] = []
    for bad_hex, bad_locations in hexes.items():
        if len(bad_locations) > 1:
            bad_hexes.append(bad_hex)
            yield "<li>{}</li><ul>{}</ul>".format(
                bad_hex,
                "".join("<li>{} {} {}</li>".format(*bad_loc) for bad_loc in bad_locations),
            )

    yield "</ul>"

    def _h3index_resolution(loc_hex: str) -> tuple[str, int]:
        h3index, resolution = loc_hex.split(",")
        return h3index, int(resolution)

    all_hexes = [_h3index_resolution(loc_hex) for location in locations.locations for loc_hex in location.hexes]

    all_trees = [
        [_h3index_resolution(loc_hex) for loc_hex in location.hexes]
        for location in locations.locations
        if len(location.hexes) > 1
    ]

    for i, map_image in enumerate(map_images):
        yield f"<h3>{map_image.name}</h3>"
        map_ = f'<img src="../maps/{Path(map_image.filename).name}" />'
        canvas = f'<canvas id="map{i}" width={map_width} height={map_height} style="position: absolute; top: 0; left: 0"></canvas>'
        draw_hexes = '<script>hexes("map{}", [{}], [{}], [{}]); trees("map{}", [{}]);</script>'.format(
            i,
            ",".join(polylines(map_image, all_hexes)),
            ",".join(polylines1(map_image, all_hexes)),
            ",".join(polylines2(map_image, bad_hexes)),
            i,
            ",".join(trees(map_image, all_trees)),
        )
        yield f'<div style="position: relative;">{map_}{canvas}{draw_hexes}</div>'

    yield MAP_HTML_POST


def polylines(map_image: MapImage, hexes: Iterable[tuple[str, int]]) -> Iterable[str]:
    for h3index, _resolution in hexes:
        hex1 = map_image.h3_points(h3index)
        if hex1:
            yield "[{}]".format(",".join(f"{int(1000*x)},{int(1000*y)}" for x, y in hex1))


def polylines1(map_image: MapImage, hexes: Iterable[tuple[str, int]]) -> Iterable[str]:
    for h3index, resolution in hexes:
        hex2 = map_image.h3_points(h3.h3_to_parent(h3index, int(resolution)))
        if hex2:
            yield "[{}]".format(",".join(f"{int(1000*x)},{int(1000*y)}" for x, y in hex2))


def polylines2(map_image: MapImage, hexes: Iterable[str]) -> Iterable[str]:
    for h3index in hexes:
        hex1 = map_image.h3_points(h3index)
        if hex1:
            yield "[{}]".format(",".join(f"{int(1000*x)},{int(1000*y)}" for x, y in hex1))


def trees(map_image: MapImage, locations: Iterable[Iterable[tuple[str, int]]]) -> Iterable[str]:
    for location in locations:
        points = [
            map_image.coord_to_frac(*h3.h3_to_geo(h3.h3_to_parent(h3index, int(resolution))))
            for h3index, resolution in location
        ]
        if any(0.0 <= x <= 1.0 or 0.0 <= y <= 1.0 for x, y in points):
            yield "[{}]".format(",".join(f"{int(1000*x)},{int(1000*y)}" for x, y in points))


MAP_HTML_PRE = """
<html>
<head><script>
var width = 1268 / 1000;
var height = 951 / 1000;

function hexes(map_id, polylines, polylines1, polylines2) {
    var ctx = document.getElementById(map_id).getContext('2d');
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'rgba(0,0,255,0.5)';
    ctx.fillStyle = 'rgba(0,0,255,0.1)';
    for(line of polylines1) {
        ctx.beginPath();
        hex_line(ctx, line);
        ctx.stroke();
        ctx.fill();
    }

    ctx.strokeStyle = 'rgba(0,127,0,0.5)';
    for(line of polylines) {
        ctx.beginPath();
        hex_line(ctx, line);
        ctx.stroke();
    }

    ctx.strokeStyle = 'red';
    for(line of polylines2) {
        ctx.beginPath();
        hex_line(ctx, line);
        ctx.stroke();
    }
}

function hex_line(ctx, line) {
    var len = line.length;
    ctx.moveTo(line[len-2] * width,
               line[len-1] * height);
    for(var i = 0; i < len; i+=2) {
        ctx.lineTo(line[i] * width,
                   line[i+1] * height);
    }
}

function trees(map_id, tree_list) {
    var ctx = document.getElementById(map_id).getContext('2d');
    ctx.lineWidth = 2;
    ctx.strokeStyle = 'rgba(0,127,0,0.5)';
    ctx.fillStyle = 'rgb(0, 127, 0)';
    ctx.font = "12px sans-serif";
    for(line of tree_list) {
        ctx.beginPath();
        ctx.moveTo(line[0]*width, line[1]*height);
        for(var i=0; i<line.length; i+=2) {
            ctx.lineTo(line[i]*width, line[i+1]*height);
        }
        ctx.stroke();
        for(var i=0; i<line.length; i+=2) {
            ctx.beginPath();
            ctx.arc(line[i]*width, line[i+1]*height,5,0,Math.PI*2);
            ctx.stroke();
            //ctx.fillText(i/2, line[i]*width + 7, line[i+1]*height);
        }
    }
}
</script></head>
<body>
"""


MAP_HTML_POST = """
</body>
</html>

"""
