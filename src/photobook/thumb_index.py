from collections.abc import Iterable
from pathlib import Path
from typing import Any

from photobook.book_util import (
    BookDict,
    PageDict,
    PhotoDict,
    _page_photos,
    concatenate_books,
)


def get_thumbnail_pages(source: dict[str, Any]) -> list[PageDict]:
    photos = ThumbGrid(source["cols"], source["rows"])

    for volume in source["books"]:
        _collect_photos(photos, concatenate_books(map(Path, volume["source"])), volume["book"])

    layout = f'g{source["cols"]}x{source["rows"]}'
    return [{"page": chunk, "layout": layout} for chunk in photos.chunks()]


class ThumbGrid:
    def __init__(self, cols: int, rows: int) -> None:
        self.slots = cols * rows
        self.cols = cols
        self.rows = rows
        self.photos: list[PhotoDict] = []

    def __len__(self) -> int:
        return len(self.photos)

    @property
    def row(self) -> int:
        return (len(self) // self.cols) % self.rows

    @property
    def col(self) -> int:
        return len(self) % self.cols

    @staticmethod
    def _blanks(n: int) -> list[PhotoDict]:
        return [{"photo": "BLANK"} for i in range(n)]

    def chunks(self) -> Iterable[list[PhotoDict]]:
        def chunk(photos: list[PhotoDict]) -> list[PhotoDict]:
            return photos + self._blanks(self.slots - len(photos))

        for i in range(0, len(self), self.slots):
            yield chunk(self.photos[i : i + self.slots])

    def new_row(self) -> None:
        if self.col:
            self.photos.extend(self._blanks(self.cols - self.col))

    def check_orphans(self, rows: int) -> None:
        # blanks until the end of the page if not enough room to start
        # the next book
        if self.rows - self.row <= rows:
            self.photos.extend(self._blanks(self.slots - (len(self) % self.slots)))

    def title(self, title: str) -> None:
        self.new_row()
        self.photos.append({"photo": "BLANK", "thumb_title": title})
        self.new_row()

    def subtitle(self, title: str | None) -> None:
        self.new_row()
        self.photos.append({"photo": "BLANK", "thumb_subtitle": title})
        self.new_row()

    def photo(self, filename: str, page_num: str | None) -> None:
        photo = {"photo": filename, "crop": "outside square top"}
        if page_num:
            photo["thumb_page"] = page_num
        self.photos.append(photo)


def _collect_photos(photos: ThumbGrid, book: BookDict, title: str) -> None:
    # include subtitle for first book if there are multiple books in volume
    titles = book.pop("titles", [])
    if len(titles) > 1:
        photos.check_orphans(3)
        photos.title(title)
        photos.subtitle(titles[0])
    else:
        photos.check_orphans(2)
        photos.title(title)

    for page_num, page in _collect_pages(book):
        if page.get("book_title"):
            # blank row for title
            photos.check_orphans(2)
            photos.subtitle(page.get("book_title"))

        for photo in _page_photos(page):
            photos.photo(photo["photo"], page_num)


def _collect_pages(book: BookDict) -> Iterable[tuple[str, PageDict]]:
    if "front" in book:
        yield "Cover", book["front"]

    if "in_front" in book:
        yield "Inside cover", book["in_front"]

    for page_num, page in enumerate(book["pages"]):
        yield str(page_num + 1), page

    if "in_back" in book:
        yield "Inside back", book["in_back"]

    if "back" in book:
        yield "Back", book["back"]
