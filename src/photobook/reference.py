# ruff: noqa: T201

import json
from collections.abc import Iterable, Iterator
from contextlib import contextmanager
from fractions import Fraction
from math import atan2, cos, degrees, sin, sqrt
from pathlib import Path
from textwrap import dedent
from typing import Any

import click
from book_layout.book_schema import BookModel, LocationModel, MapModel, MapsModel
from page_layout import ALL_LAYOUTS, ASPECTS, LayoutParams

from photobook.reconcilers import Reconciler

md_base_template = """
## {command_name}

{description}

### Usage

```
{usage}
```

| Options |      |      |
| ------- | ---- | ---- |
{options}

"""


def recursive_help(cmd: click.core.Command, parent: click.core.Context | None = None) -> Iterable[dict[str, Any]]:
    ctx = click.core.Context(cmd, info_name=cmd.name, parent=parent)

    yield {
        "command": cmd,
        "parent": parent.info_name if parent else "",
        "usage": cmd.get_usage(ctx),
        "params": cmd.get_params(ctx),
        "options": cmd.collect_usage_pieces(ctx),
    }

    commands = getattr(cmd, "commands", {})
    for sub in commands.values():
        yield from recursive_help(sub, ctx)


def _type_desc(opt_type: Any) -> str:
    if isinstance(opt_type, click.types.Path):
        return "FILE"

    if isinstance(opt_type, click.types.StringParamType):
        return "TEXT"

    if isinstance(opt_type, click.types.BoolParamType):
        return ""

    return str(opt_type)


def print_md_help(base_command: click.core.Command) -> None:
    """Print help usage in md format from Click commands"""
    for helpdct in recursive_help(base_command):
        command = helpdct["command"]
        usage = helpdct["usage"]
        parent = helpdct["parent"]
        options = {
            opt.name: {
                "usage": "  ".join(opt.opts),
                "required": opt.required,
                "default": opt.default,
                "help": getattr(opt, "help", ""),
                "type": _type_desc(opt.type),
            }
            for opt in helpdct["params"]
            if not isinstance(opt, click.core.Argument)
        }
        full_command = f"{parent}{' ' if parent else ''}{command.name}"

        md_template = md_base_template.format(
            command_name=full_command,
            description=dedent(command.help or ""),
            usage=usage.replace("Usage: ", ""),
            options="\n".join(
                [
                    f"| `{opt.get('usage')}`"
                    f" | {opt.get('type')} "
                    f"{' (REQUIRED)' if opt.get('required') else ''}"
                    # f" | `{str(opt.get('default')).lower()}`"
                    f" | {opt.get('help') or ''}"
                    f" |"
                    for opt_name, opt in options.items()
                ]
            ),
        )

        print(md_template)


def print_reference_cli(cli: click.core.Command) -> None:
    print("<!-- AUTOMATICALLY GENERATED -- DO NOT EDIT -->")
    print("<!-- photobook reference cli > 'docs/Command Line Reference.md' -->")
    print()
    print("# Command Line Reference")

    print(
        dedent(
            """
    Primary commands:
      - `photobook make`

    Sub-commands for specific steps:
      - `photobook generate book`
      - `photobook annotate`
      - `photobook generate index`
      - `photobook generate thumb-index`
      - `photobook render`
      - `photobook map-preview`

    Reference commands for self-documentation:
      - `photobook reference aspects`
      - `photobook reference cli`
      - `photobook reference layouts`
      - `photobook reference schema`
      - `photobook make --visualize`
    """
        )
    )

    print_md_help(cli)


class DotGraph:
    def __init__(self) -> None:
        self.nodes: dict[str, str] = {}
        self.edges: list[tuple[str, str]] = []

    def add_node(self, key: str, label: str, shape: str = "note") -> str:
        self.nodes[key] = f'[label="{label}" shape={shape}]'
        return key

    def add_edge(self, key1: str, key2: str) -> None:
        self.edges.append((key1, key2))

    def output(self) -> str:
        edges = "\n  ".join(f"{a}->{b}" for (a, b) in self.edges)
        nodes = "\n  ".join(f"{key} {node}" for key, node in self.nodes.items())
        return "digraph {\n  " + nodes + "\n  " + edges + "\n}\n"


class ReconcilerGraph:
    def __init__(self) -> None:
        self.g = DotGraph()
        self.output = self.g.output
        self.add_edge = self.g.add_edge

        self.keys = [chr(65 + i) for i in range(26)]
        self.files: dict[str, str] = {}

    def add_reconciler(self, reconciler: str) -> str:
        return self.g.add_node(self.keys.pop(), reconciler, shape="diamond")

    def add_file(self, filename: str) -> str:
        if filename not in self.files:
            self.files[filename] = self.keys.pop()
        return self.g.add_node(self.files[filename], filename)


def render_reconciler_graph(reconciler_list: list[Reconciler]) -> str:
    g = ReconcilerGraph()
    for reconciler in reconciler_list:
        key = g.add_reconciler(reconciler.name)
        for in_file in reconciler.input_files:
            g.add_edge(g.add_file(str(in_file)), key)
        for out_file in reconciler.output_files:
            g.add_edge(key, g.add_file(str(out_file)))
        if hasattr(reconciler, "reconciler"):
            reconciler_list.append(reconciler.reconciler)
    return g.output()


def generate_reference_aspects() -> tuple[list[str], str]:
    aspect_table = []
    colours = [
        "blue",
        "violet",
        "red",
        "darkgreen",
        "green",
        "orange",
        "gray",
        "black",
        "brown",
    ] * 2

    svg = SVG()
    with svg.document(width=1000, viewbox=(-125, -125, 250, 250)):
        left = False
        for aspect, colour in zip(ASPECTS, colours, strict=False):
            aspect_table.append(f"| `{aspect.code:<16}` | {aspect.description:<40} |")

            left = not left
            area = 100 * 100 * 0.5
            width = round(sqrt(area * aspect.landscape))
            height = round(width / aspect.landscape)
            svg.rect(
                -height,
                -width,
                height * 2,
                width * 2,
                colour=colour,
                stroke_width=1,
                fill="rgba(255, 255, 255, 0.5)",
            )
            svg.rect(
                -width,
                -height,
                width * 2,
                height * 2,
                colour=colour,
                stroke_width=1,
                fill="rgba(255, 255, 255, 0.5)",
            )
            svg.text(
                # alternate position to each side in turn
                (width + 1) * (-1 if left else 1),
                -(height + 1),
                "end" if left else "start",
                fill=colour,
                content=aspect.code,
                style="",
                font_size=5,
            )

    return aspect_table, svg.result


def generate_reference_layouts() -> Iterable[tuple[str, str, str]]:
    params = LayoutParams(page_width=Fraction(100), gutter=Fraction(5))

    for layout in ALL_LAYOUTS:
        match len(layout.slots):
            case 1 | 2:
                # list in full for one or two slots
                orientations = ", ".join(slot.orientation for slot in layout.slots)
            case 3 | 4 | 5 | 6:
                # abbreviate for a small number of slots
                orientations = ", ".join(slot.orientation[0].upper() for slot in layout.slots)
            case _:
                # just a count of the slots
                orientations = f"{layout.slots[0].orientation} ({len(layout.slots)})"

        svg_name = f"images/reference-layout-{layout.layout_id}.svg"
        layout_table = f"| {layout.layout_id:<16} | {orientations:<40} | ![{layout.layout_id} thumbnail]({svg_name}) |"

        svg = SVG()
        with svg.document(width=250, viewbox=(-10, -20, 117, 127)):
            svg.rect(0, 0, 100, 100, "silver", fill="white", stroke_width=1)
            svg.text(
                0,
                -3,
                text_anchor="start",
                fill="gray",
                content=layout.layout_id,
                font_family="monospace",
                font_size=10,
                style="",
            )

            for slot in layout.slots:
                rect = slot.get_rect(params)
                svg.rect(
                    float(rect.left),
                    float(rect.top),
                    float(rect.width),
                    float(rect.height),
                    "black",
                    stroke_width=1,
                    fill="rgba(192, 192, 192, 0.5)",
                )

        yield layout_table, svg_name, svg.result


def print_md_schema(schema: dict[str, Any], **extra_content: str) -> None:
    def _type(details: dict[str, Any]) -> str | None:
        if "allOf" in details:
            t = _type(details["allOf"][0])
            if t is None:
                raise ValueError
            return "$" + t

        if "type" in details:
            if details["type"] == "array":
                return f'[array of {_type(details["items"])}]'

            t = details["type"]
            if not isinstance(t, str):
                raise TypeError(t)
            return t.replace("null", "None")

        if "anyOf" in details:
            return " | ".join(_type(t) or "" for t in details["anyOf"])

        if "oneOf" in details:
            return " | ".join(_type(t) or "" for t in details["oneOf"])

        if "$ref" in details:
            r = details["$ref"]
            if not isinstance(r, str):
                raise TypeError(r)
            return r.split("/")[-1]

        return None

    def _value(value: Any, type_: str | None) -> str:
        if value is None:
            value = "None"

        if type_ == "string":
            value = f'"{value}"'

        if type_ in ("string", "boolean", "integer"):
            return str(value)

        if type_ is not None:
            if " | " in type_:
                return type_

            if type_.startswith("$"):
                return type_[1:]

            if type_.startswith("["):
                return type_

        return f"{value} ({type_})"

    for prop, details in schema["properties"].items():
        print(f'### {details.get("title")}')

        print(f'```\n{prop}: {_value(details.get("default"), _type(details))}\n```')

        if "description" in details:
            description = details["description"]
            for key in extra_content:
                if key in description:
                    description = description.replace(key, extra_content[key])
            print(description)

        # # for debugging, output the remaining schema details:
        # details.pop("title", None)
        # details.pop("description", None)
        # details.pop("examples", None)
        # details.pop("default", None)
        # details.pop("anyOf", None)
        # details.pop("allOf", None)
        # if details.get("type") in ("string", "number", "boolean", "integer"):
        #     details.pop("type", None)
        # if details.get("type") in ("array",):
        #     details.pop("type", None)
        #     details.pop("items", None)
        # if details.get("type") in ("object",):
        #     details.pop("type", None)
        #     details.pop("additionalProperties", None)
        # if details:
        #     print("```json\n" + dumps(details, indent=2) + "\n```")

        print()


def print_reference_schema() -> None:
    print("<!-- AUTOMATICALLY GENERATED -- DO NOT EDIT -->")
    print("<!-- photobook reference schema > 'docs/Photobook File Formats.md' -->")
    print()
    # TODO: print intro

    # NOTE: this isn't updating the actual svg images...
    aspect_table, aspect_svg = generate_reference_aspects()
    layout_table = [layout_table_ for layout_table_, svg_name, layout_svg in generate_reference_layouts()]

    content = {
        "ASPECT_RATIO_TABLE_PLACEHOLDER": md_aspect_table(aspect_table),
        "LAYOUT_TABLE_PLACEHOLDER": md_layout_table(layout_table),
    }

    print("# Photobook File Format")
    print()

    schema = BookModel.model_json_schema(by_alias=True)
    p = Path("schema/book-schema.json")
    p.parent.mkdir(exist_ok=True)
    with p.open("w") as f:
        json.dump(schema, f, indent=4)

    print("## Book schema")
    print()
    print_md_schema(schema)

    print("## Book params")
    print_md_schema(schema["$defs"]["BookParams"])

    print("## Page schema")
    print()
    print_md_schema(schema["$defs"]["PageModel"], **content)

    print("## Photo schema")
    print()
    print_md_schema(schema["$defs"]["PhotoModel"])

    print("## Map Index schema")
    print()
    print_md_schema(schema["$defs"]["MapContentModel"])

    print("## Label schema")
    print_md_schema(schema["$defs"]["LabelModel"])

    print("## Calendar Index schema")
    print()
    print_md_schema(schema["$defs"]["CalendarModel"])

    print("# Thumbnail Index File Format")
    print()
    print_md_schema(schema["$defs"]["ThumbPhotoModel"])

    schema = MapsModel.model_json_schema(by_alias=True)

    print("# Map File Format")
    print()
    print_md_schema(schema)
    print("## Map schema")
    print()
    print(dedent(MapModel.__doc__ or ""))
    print()
    print_md_schema(schema["$defs"]["MapModel"])
    print("## Location schema")
    print()
    print(dedent(LocationModel.__doc__ or ""))
    print()
    print_md_schema(schema["$defs"]["LocationModel"])
    print()


def md_aspect_table(aspect_table: list[str]) -> str:
    return f'| {"Aspect code":<16} | {"Description":<40} |\n| {"-"*16} | {"-"*40 } |\n' + "\n".join(aspect_table)


def md_layout_table(layout_table: list[str]) -> str:
    return (
        f'| {"Layout code":<16} | {"Photo orientations":<40} |     |\n'
        f'| {"-"*16} | {"-"*40 } | --- |\n' + "\n".join(layout_table)
    )


class SVG:
    def __init__(
        self,
        maxx: int = 200,
        maxy: int = 150,
        w0: int = 200,
        h0: int = 150,
        pad: int = 20,
    ) -> None:
        # svg coords
        self.pad = pad
        self.w2 = w0 - 2 * self.pad
        self.h2 = h0 - 2 * self.pad

        # pixels
        self.maxx = maxx
        self.maxy = maxy
        if self.maxx / self.maxy > w0 / h0:
            self.w1: float = self.maxx
            self.h1: float = h0 * self.w1 / w0
        else:
            self.h1 = self.maxy
            self.w1 = w0 * self.h1 / h0

        self.result = ""

    # document

    def _write(self, xml: str) -> None:
        self.result += xml + "\n"

    def write(self, tag: str, content: str = "", **attrs: Any) -> None:
        attrs_str = " ".join('{}="{}"'.format(k.replace("_", "-"), v) for k, v in attrs.items())
        if content:
            self._write(f"<{tag} {attrs_str}>{content}</{tag}>")
        else:
            self._write(f"<{tag} {attrs_str} />")

    @contextmanager
    def document(
        self,
        width: int,
        viewbox: tuple[float, float, float, float],
    ) -> Iterator[None]:
        viewbox_str = " ".join(map(str, viewbox))
        self._write(f'<svg width="{width}" viewBox="{viewbox_str}" xmlns="http://www.w3.org/2000/svg">')
        yield
        self._write("</svg>")

    # pixels to coords

    def xx(self, x: float) -> float:
        return self.pad + x * self.w2 / self.w1

    def yy(self, y: float) -> float:
        return self.pad + y * self.h2 / self.h1

    def center(self, x: float, y: float, width: float, height: float) -> tuple[float, float]:
        return x + width / 2, y + height / 2

    # drawing

    def rect(
        self,
        x: float,
        y: float,
        width: float,
        height: float,
        colour: str,
        stroke_width: float = 0.4,
        fill: str = "none",
        **attrs: Any,
    ) -> None:
        self.write(
            "rect",
            x=x,
            y=y,
            width=width,
            height=height,
            style=f"fill: {fill}; stroke: {colour}; stroke-width: {stroke_width};",
            **attrs,
        )

    def line(
        self,
        x1: float,
        y1: float,
        x2: float,
        y2: float,
        colour: str,
        stroke_width: float = 0.2,
        **attrs: Any,
    ) -> None:
        self.write(
            "line",
            x1=x1,
            y1=y1,
            x2=x2,
            y2=y2,
            stroke=colour,
            stroke_width=stroke_width,
            **attrs,
        )

    def circle(
        self,
        cx: float,
        cy: float,
        r: float,
        colour: str,
        fill: str = "white",
        stroke_width: float = 0.2,
        **attrs: Any,
    ) -> None:
        self.write(
            "circle",
            cx=cx,
            cy=cy,
            r=r,
            stroke=colour,
            fill=fill,
            stroke_width=stroke_width,
            **attrs,
        )

    def text(
        self,
        x: float,
        y: float,
        text_anchor: str = "middle",
        fill: str = "black",
        content: str = "text",
        style: str = "font-family: monospace; font-size: 4px",
        **attrs: Any,
    ) -> None:
        self.write(
            "text",
            style=style,
            text_anchor=text_anchor,
            x=x,
            y=y,
            fill=fill,
            content=content,
            **attrs,
        )

    def polyline(
        self,
        colour: str,
        points: list[tuple[float, float]],
        stroke_width: float = 0.2,
        stroke_dasharray: Any = 2,
        fill: str = "none",
        **attrs: Any,
    ) -> None:
        self.write(
            "polyline",
            fill=fill,
            stroke_width=stroke_width,
            stroke_dasharray=stroke_dasharray,
            stroke=colour,
            points=" ".join("{},{}".format(*p) for p in points),
            **attrs,
        )

    def image_rect(
        self,
        label: str,
        colour: str,
        colour_light: str,
        size: tuple[float, float, float, float],
        align: str,
        *,
        origin: bool = True,
        center: bool = True,
    ) -> None:
        x, y, width, height = size

        xx = self.xx
        yy = self.yy

        self.rect(
            xx(x),
            yy(y),
            xx(width) - xx(0),
            yy(height) - yy(0),
            colour,
        )

        self.line(xx(x), yy(y), xx(x + width), yy(y + height), colour_light)
        self.line(xx(x), yy(y + height), xx(x + width), yy(y), colour_light)

        if align == "left":
            x1 = xx(x) + 3
            text_anchor = "start"
        elif align == "center":
            x1 = xx(x + width / 2)
            text_anchor = "middle"
        elif align == "right":
            x1 = xx(x + width) - 3
            text_anchor = "end"

        self.text(x1, yy(y + height) - 3, text_anchor=text_anchor, fill=colour, content=label)

        if origin:
            self.circle(xx(x), yy(y), 1, colour)

        if center:
            cx, cy = self.center(*size)
            self.circle(xx(cx), yy(cy), 1, colour)

    def dimension(
        self,
        label: str,
        position: str,
        colour: str,
        start: float,
        stop: float,
    ) -> None:
        xx = self.xx
        yy = self.yy

        if position == "top":
            self._dimension(
                label,
                colour,
                [(xx(start), 14), (xx(start), 15), (xx(stop), 15), (xx(stop), 14)],
                x=xx((start + stop) / 2),
                y=yy(0) - 7,
            )

        elif position == "left":
            self._dimension(
                label,
                colour,
                [(14, yy(start)), (15, yy(start)), (15, yy(stop)), (14, yy(stop))],
                transform=f"rotate(-90) translate(-{yy((start+stop)/2)}, 12)",
            )

        elif position == "bottom":
            self._dimension(
                label,
                colour,
                [
                    (xx(start), yy(self.maxy) + 5),
                    (xx(start), yy(self.maxy) + 4),
                    (xx(stop), yy(self.maxy) + 4),
                    (xx(stop), yy(self.maxy) + 5),
                ],
                x=xx((start + stop) / 2),
                y=yy(self.maxy) + 10,
            )

        elif position == "right":
            self._dimension(
                label,
                colour,
                [
                    (xx(self.maxx) + 6, yy(start)),
                    (xx(self.maxx) + 5, yy(start)),
                    (xx(self.maxx) + 5, yy(stop)),
                    (xx(self.maxx) + 6, yy(stop)),
                ],
                transform=(f"rotate(90) translate({yy((start+stop)/2)}, -{xx(self.maxx)+10})"),
            )

    def _dimension(
        self,
        label: str,
        colour: str,
        points: list[tuple[float, float]],
        x: float = 0,
        y: float = 0,
        transform: str = "",
    ) -> None:
        self.polyline(colour, points)
        self.text(x, y, transform=transform, content=label)

    def arrow(self, colour: str, x1: float, y1: float, x2: float, y2: float) -> None:
        xx = self.xx
        yy = self.yy
        angle = atan2((y2 - y1), (x2 - x1))
        dx, dy = cos(angle), sin(angle)
        self.line(xx(x1), yy(y1), xx(x2), yy(y2), colour, stroke_dasharray=1)
        self.polyline(
            colour,
            [(-1, -0.5), (0, 0), (-1, 0.5)],
            stroke_dasharray="",
            transform=f"translate({xx(x2)-dx}, {yy(y2)-dy}) scale(1.5) rotate({degrees(angle)})",
            fill=colour,
        )
