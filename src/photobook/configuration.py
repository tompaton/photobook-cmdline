import importlib.resources
import os
import tomllib
from dataclasses import dataclass
from pathlib import Path


@dataclass
class Configuration:
    MAPBOX_KEY: str = ""
    font_path: str = ""
    map_path: Path = Path()

    @classmethod
    def load(cls) -> "Configuration":
        base_path, config = find_config(Path.cwd())

        default_font_path = str(importlib.resources.files("photobook.fonts").joinpath(" ")).strip()
        font_path = config.get("font_path", default_font_path)

        default_map_path = base_path / "maps"
        map_path = Path(config.get("map_path", str(default_map_path)))

        if "MAPBOX_KEY" in os.environ:
            return cls(os.environ["MAPBOX_KEY"], font_path, map_path)

        try:
            s = config["MAPBOX_KEY"]
        except KeyError:
            pass
        else:
            if isinstance(s, str):
                return cls(s, font_path, map_path)

        return cls("", font_path, map_path)


def find_config(base_path: Path, start_path: Path | None = None) -> tuple[Path, dict[str, str]]:
    if not base_path.parent:
        if start_path is None:
            raise AssertionError  # huh?
        return start_path, {}

    config_file = base_path / "photobook.toml"
    if config_file.exists():
        return base_path, tomllib.loads(config_file.read_text())

    return find_config(base_path.parent, start_path or base_path)
