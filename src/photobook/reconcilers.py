from collections.abc import Iterable
from pathlib import Path
from typing import Any

from book_layout.book_pdf import RenderMode
from rich.console import Console
from rich.status import Status

from photobook import operations


class Reconciler:
    name: str
    output_file: Path

    def can_update(self) -> bool:
        return False

    def update(self, console: Console, status: Status) -> None:
        pass

    @property
    def input_files(self) -> Iterable[Path]:
        return []

    @property
    def output_files(self) -> Iterable[Path]:
        return []


class MapPreview(Reconciler):
    def __init__(self, maps_file: Path, map_html: Path, *, launch: bool = False, **kwargs: Any) -> None:
        self.name = "Map Preview"
        self.maps_file = maps_file
        self.output_file = map_html
        self.launch = launch
        self.kwargs = kwargs

    def can_update(self) -> bool:
        return is_newer(self.maps_file, self.output_file)

    def update(self, console: Console, _status: Status) -> None:
        operations.do_map_preview(console, self.maps_file, self.output_file, launch=self.launch, **self.kwargs)
        self.launch = False  # only do this the first time

    @property
    def input_files(self) -> Iterable[Path]:
        yield self.maps_file

    @property
    def output_files(self) -> Iterable[Path]:
        yield self.output_file


class GenerateBook(Reconciler):
    def __init__(self, photo_path: Path) -> None:
        self.name = "Generate Book"
        self.photo_path = photo_path
        self.output_file = with_suffix([self.photo_path], "-", ".yml")

    def can_update(self) -> bool:
        return is_missing(self.output_file)

    def update(self, console: Console, status: Status) -> None:
        operations.do_generate_book(console, status, self.photo_path, self.output_file)

    @property
    def input_files(self) -> Iterable[Path]:
        yield self.photo_path

    @property
    def output_files(self) -> Iterable[Path]:
        yield self.output_file


class Annotate(Reconciler):
    def __init__(self, map_yml: Path, *book_yml: Path) -> None:
        self.name = "Annotate"
        self.book_yml = list(book_yml)
        self.map_yml = map_yml
        self.output_file = with_suffix(self.book_yml, "-", "-unknown-locations.yml")

    def can_update(self) -> bool:
        return any_is_newer([*self.book_yml, self.map_yml], [self.output_file])

    def update(self, console: Console, _status: Status) -> None:
        operations.do_annotate(console, self.book_yml, self.map_yml, unknown=self.output_file)

    @property
    def input_files(self) -> Iterable[Path]:
        yield self.map_yml

    @property
    def output_files(self) -> Iterable[Path]:
        yield from self.book_yml
        yield self.output_file


class GenerateIndex(Reconciler):
    def __init__(self, map_yml: Path, *book_yml: Path, **kwargs: Any) -> None:
        self.name = "Generate Index"
        self.map_yml = map_yml
        self.book_yml = list(book_yml)
        self.output_file = with_suffix(self.book_yml, "-", "-index.yml")
        self.kwargs = kwargs

    def can_update(self) -> bool:
        return is_missing(self.output_file) or any_is_newer([self.map_yml, *self.book_yml], [self.output_file])

    def update(self, console: Console, _status: Status) -> None:
        operations.do_generate_index(console, self.book_yml, self.map_yml, self.output_file, **self.kwargs)

    @property
    def input_files(self) -> Iterable[Path]:
        yield self.map_yml
        yield from self.book_yml

    @property
    def output_files(self) -> Iterable[Path]:
        yield self.output_file


class KeepManualEdits(Reconciler):
    """
    Wrap a Reconciler with a check to ensure it only runs if it's
    output hasn't been modified since it was generated.
    """

    def __init__(self, timestamp_file: Path, reconciler: Reconciler) -> None:
        self.name = "Keep Manual Edits"
        self.timestamp_file = timestamp_file
        self.reconciler = reconciler

    def can_update(self) -> bool:
        return not is_newer(self.reconciler.output_file, self.timestamp_file) and self.reconciler.can_update()

    def update(self, console: Console, status: Status) -> None:
        self.reconciler.update(console, status)
        self.timestamp_file.touch()

    @property
    def input_files(self) -> Iterable[Path]:
        yield self.timestamp_file
        yield self.reconciler.output_file

    @property
    def output_files(self) -> Iterable[Path]:
        yield self.timestamp_file


class Render(Reconciler):
    def __init__(self, name: str, render_mode: RenderMode, *book_yml: Path, **kwargs: Any) -> None:
        self.name = name
        self.render_mode = render_mode
        self.kwargs = kwargs
        self.book_yml = list(book_yml)

        self.output_file_in = with_suffix(self._yml_not_index, "-", ".pdf")
        if not render_mode.cover:
            self.output_file = append_to_stem(self.output_file_in, " (pages)")
        elif not render_mode.pages:
            self.output_file = append_to_stem(self.output_file_in, " (cover)")
        else:
            self.output_file = self.output_file_in

    def can_update(self) -> bool:
        return any_is_newer(self.book_yml, [self.output_file])

    def update(self, console: Console, _status: Status) -> None:
        operations.do_render(
            console,
            self.book_yml,
            self.render_mode,
            out=self.output_file_in,
            **self.kwargs,
        )
        self.kwargs["launch"] = False  # only do this the first time

    @property
    def input_files(self) -> Iterable[Path]:
        yield from self.book_yml

    @property
    def _yml_not_index(self) -> list[Path]:
        if len(self.book_yml) == 1:
            return self.book_yml

        # exclude index from pdf filename
        return [f for f in self.book_yml if "index" not in f.stem]

    @property
    def output_files(self) -> Iterable[Path]:
        yield self.output_file


def with_suffix(files: list[Path], separator: str, new_stem: str) -> Path:
    return files[0].parent / (separator.join(f.stem for f in files) + new_stem)


def append_to_stem(f: Path, stem_suffix: str) -> Path:
    return f.with_name(f"{f.stem}{stem_suffix}{f.suffix}")


def is_missing(file1: Path) -> bool:
    return not file1.exists()


def is_newer(file1: Path, file2: Path) -> bool:
    if is_missing(file1):
        return False

    if is_missing(file2):
        return True

    return file1.stat().st_mtime > file2.stat().st_mtime


def any_is_newer(files1: Iterable[Path], files2: Iterable[Path]) -> bool:
    return any(is_newer(file1, file2) for file1 in files1 for file2 in files2)
