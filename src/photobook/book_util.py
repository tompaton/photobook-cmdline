from collections.abc import Iterable
from pathlib import Path
from typing import Any, TextIO

from book_layout import book_pdf
from ruyaml import YAML

BookDict = dict[str, Any]
PageDict = dict[str, Any]
PhotoDict = dict[str, Any]


def read_yaml(f: Path | TextIO) -> Any:
    yaml = YAML(typ="rt")
    if isinstance(f, Path):
        f = f.open()
    return yaml.load(f)


def write_yaml(
    f: Path | TextIO,
    yaml_data: Any,
    *,
    close: bool = True,
    add_schema: str | None = None,
) -> None:
    yaml = YAML(typ="rt")
    if isinstance(f, Path):
        f = f.open("wt")
    if add_schema is not None:
        f.write(add_schema)
    if close:
        with f:
            yaml.dump(yaml_data, f)
    else:
        yaml.dump(yaml_data, f)


def concatenate_books(input_yaml: Iterable[Path]) -> BookDict:
    volume: BookDict = {}
    titles = []
    for input_yaml_ in input_yaml:
        book = fix_photo_paths(read_yaml(input_yaml_), input_yaml_.parent)
        titles.append(book.get("book_title", "Untitled"))
        volume = book_pdf.concatenate_books(volume, book)
    volume["titles"] = titles
    return volume


def fix_photo_paths(book: BookDict, book_path: Path) -> BookDict:
    for _page_num, page in _all_pages(book):
        if not page.get("page"):
            page["page"] = []
        for photo in page["page"]:
            filename = photo.get("photo", "BLANK")
            if filename == "BLANK":
                continue

            photo["photo"] = fix_photo_path(filename, book_path)

    return book


def fix_photo_path(filename: str, book_path: Path) -> str:
    if not Path(filename).exists():
        filename = str(book_path / filename)

    if (
        not Path(filename).exists()
        and filename.startswith("/uploads")
        and Path(filename.replace("/uploads", ".")).exists()
    ):
        filename = filename.replace("/uploads", ".")

    return filename


def _all_pages(book: BookDict) -> Iterable[tuple[str, PageDict]]:
    for mode in ("front", "in_front", "in_back", "back"):
        if mode in book:
            yield mode, book.get(mode) or {}

    for i, page in enumerate(book.get("pages", [])):
        page_num = f"page {i + 1}"
        yield page_num, page


def _page_photos(page: PageDict) -> list[PhotoDict]:
    return [photo for photo in page.get("page", []) if not photo.get("skip")]


def _all_page_photos(
    book: BookDict,
) -> list[tuple[PageDict, PhotoDict]]:
    return [(page, photo) for page_num, page in _all_pages(book) for photo in _page_photos(page)]


def _all_photos(book: BookDict) -> list[PhotoDict]:
    return [photo for page, photo in _all_page_photos(book)]


def even_odd(page_num: str) -> str:
    covers = {
        "front": "odd",
        "in_front": "even",
        "in_back": "odd",
        "back": "even",
        "": "even",
    }
    if page_num in covers:
        return covers[page_num]

    page_num2 = int(page_num.replace("page ", "")) - 1
    return "odd" if bool(page_num2 % 2) else "even"


def page_num2(page_num: str) -> str:
    return {
        "front": "cover",
        "back": "back",
        "in_front": "inside front",
        "in_back": "inside back",
    }.get(page_num, page_num.replace("page ", ""))
