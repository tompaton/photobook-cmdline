import contextlib
import io
import sys
from collections import defaultdict
from collections.abc import Iterator
from operator import attrgetter
from pathlib import Path
from typing import TextIO

import rich_click as click
from book_layout import book_pdf
from book_layout.book_layouts import get_book_layout
from book_layout.book_schema import BookModel, MapsModel
from page_layout import Params
from rich.console import Console
from rich.status import Status

from photobook.book_util import (
    BookDict,
    _all_pages,
    _all_photos,
    _page_photos,
    concatenate_books,
    read_yaml,
    write_yaml,
)
from photobook.cal_index import get_cal_index, get_cals
from photobook.configuration import Configuration
from photobook.map_index import (
    Annotations,
    annotate_h3index,
    build_map_index,
    get_location_index,
    get_map_image,
    get_map_index,
    get_map_index_builder,
    lookup_photo_datetimes,
    lookup_photo_locations,
)
from photobook.map_preview import print_map_preview_html
from photobook.terminal_ui import log_panel, new_book_details
from photobook.thumb_index import get_thumbnail_pages


def configure() -> Configuration:
    config = Configuration.load()
    book_pdf.configure_reportlab(None, config.font_path)
    return config


def do_generate_book(console: Console, status: Status, photo_path: Path, out: Path) -> None:
    photos = book_pdf.PhotoDetails.load_photos(photo_path)
    photo_dict = {p.filename: p for p in photos}

    log_panel(
        console,
        f"Photos: {len(photos)}",
        title=f"Generating book {photo_path} --> {out}",
    )

    # prompt for book title and whether to auto-generate captions from photo dates
    book_title, auto_captions = new_book_details(console, status)

    raw = io.StringIO()
    write_yaml(
        raw,
        book_pdf.generate_book(
            photos,
            auto_captions=auto_captions,
            book_title=book_title,
            book_spine=book_title,
        ),
        close=False,
    )

    raw.seek(0)
    book = book_pdf.concatenate_books({}, read_yaml(raw))

    write_yaml(
        out,
        book_pdf.annotate(book, photo_dict),
        add_schema="# yaml-language-server: $schema=schema/book-schema.json\n\n",
    )


def read_maps(maps_file: Path) -> MapsModel:
    return MapsModel(**read_yaml(maps_file))


def do_annotate(
    console: Console,
    book_files: list[Path],
    maps_file: Path | None,
    *,
    unknown: Path | None = None,
    dates: bool = False,
) -> None:
    what = []
    if dates:
        what.append("Dates")
    if maps_file:
        what.append(f"Maps: {maps_file}")

    log_panel(
        console,
        "\n".join(what),
        title=f"Annotating {' '.join(map(str, book_files))}",
    )

    # dict of key: value to annotate back into the source yaml
    to_annotate: Annotations = defaultdict(dict)

    if maps_file:
        maps_model = read_maps(maps_file)
        unknown_locations = lookup_photo_locations(
            to_annotate,
            _all_photos(concatenate_books(book_files)),
            get_location_index(maps_model.locations),
        )

    if dates:
        lookup_photo_datetimes(to_annotate, _all_photos(concatenate_books(book_files)))

    annotate_h3index(book_files, to_annotate)

    if unknown:
        write_yaml(unknown, unknown_locations.model_dump(exclude_defaults=True))

        if unknown_locations.locations:
            log_panel(
                console,
                unknown.read_text(),
                title="Unknown locations",
                style="warning",
            )


def do_generate_index(
    console: Console,
    book_files: list[Path],
    maps_file: Path,
    out: Path,
    *,
    insert_blanks: bool = False,
    config: Configuration,
) -> None:
    log_panel(
        console,
        f"Maps: {maps_file}\nBooks: {' + '.join(map(str, book_files))}",
        title=f"Generating index --> {out}",
    )

    maps_model = read_maps(maps_file)

    book = concatenate_books(book_files)
    book.pop("titles", None)

    index = _generate_index(book, maps_model, insert_blanks=insert_blanks, config=config)

    write_yaml(out, index)


def _generate_index(
    book: BookDict,
    maps_model: MapsModel,
    *,
    insert_blanks: bool = False,
    config: Configuration,
) -> BookDict:
    location_index = get_location_index(maps_model.locations)

    # load PhotoDetails - avoid repeatedly calling for cal and map*2
    all_photos = _all_photos(book)
    photo_dict = book_pdf.PhotoDetails.load_photo_dict(photo["photo"] for photo in all_photos if "photo" in photo)

    # don't think page_count matters here as we don't care about the spine
    book_model = BookModel(**book)
    size = get_book_layout(book_model.format, page_count=20)
    book_params = Params.from_book_dict(book_model.params.model_dump())
    default_options = book_params.options()
    builder, label_width, marker_radius_cm, marker_radius_px = get_map_index_builder(
        size, default_options, maps_model.maps, get_cals(all_photos), config
    )

    build_map_index(builder, _all_pages(book), location_index, book_model, photo_dict)

    # generate map index
    index: BookDict = {
        "params": {"gutter": 0.5},
        "pages": (
            list(get_map_index(builder, label_width, marker_radius_cm, marker_radius_px)) + list(get_cal_index(builder))
        ),
    }

    # insert blank pages if necessary
    if insert_blanks:
        page_count = len(book.get("pages", []))
        index_count = len(index["pages"])
        fill = (page_count + index_count) % size.page_multiple
        if fill:
            for _ in range(size.page_multiple - fill):
                index["pages"].insert(
                    0,
                    {
                        "page": [],
                        "layout": "l1",
                        "meta": "blank page automatically inserted",
                    },
                )

    return index


def do_render(
    console: Console,
    book_files: list[Path],
    render_mode: book_pdf.RenderMode,
    *,
    out: Path | None = None,
    title: str | None = None,
    spine: str | None = None,
    launch: bool = False,
    no_warning: bool = False,
    unknown_yml: Path | None = None,
) -> None:
    out = _get_out_filename(out or book_files[0].with_suffix(".pdf"), render_mode)

    book = concatenate_books(book_files)
    _set_titles(book, title, spine)

    photo_dict = book_pdf.PhotoDetails.load_photo_dict(
        photo["photo"]
        for photo in _all_photos(book)
        # ignore cal/maps
        if "photo" in photo
    )

    log_panel(
        console,
        "\n".join(_get_content_summary(book)),
        title=f"Rendering {' + '.join(map(str, book_files))} --> {out}",
    )

    pdf_data = book_pdf.render_book(book, render_mode, photo_dict)

    with contextlib.suppress(book_pdf.InvalidLayoutError):
        book_pdf.annotate(book, photo_dict)

    if book.get("warning") and not no_warning:
        log_panel(
            console,
            book["warning"],
            title="Warning",
            style="warning",
        )

    notes = list(_collect_notes(book))

    if notes:
        log_panel(
            console,
            "\n".join(notes),
            title="Notes",
            style="warning",
        )

    if len(book_files) == 1 and render_mode.annotate:
        write_yaml(book_files[0], book)

        if unknown_yml:
            unknown_yml.touch()

    # do this after annotating so the timestamps are not messed up
    with out.open("wb") as f:
        f.write(pdf_data)

    if launch:
        console.log("Opening PDF...")
        click.launch(str(out))


def _get_out_filename(out: Path, render_mode: book_pdf.RenderMode) -> Path:
    if not render_mode.cover and not render_mode.pages:
        msg = "Must output cover and/or pages"
        raise ValueError(msg)

    if not render_mode.pages:
        return out.with_name(out.stem + " (cover)" + out.suffix)

    if not render_mode.cover:
        return out.with_name(out.stem + " (pages)" + out.suffix)

    return out


def _set_titles(book: BookDict, title: str | None, spine: str | None) -> None:
    book.pop("titles", None)

    if title is not None:
        book["book_title"] = title

    if spine is not None:
        book["book_spine"] = spine


def _get_content_summary(book: BookDict) -> Iterator[str]:
    yield f"Title: {book.get('book_title', 'Untitled')}"
    yield f"Spine: {book.get('book_spine', 'None')}"

    page_count = len(book.get("pages", []))
    photo_count = len(_all_photos(book))
    yield f"Pages: {page_count}"
    yield f"Photos: {photo_count}"


def _collect_notes(book: BookDict) -> Iterator[str]:
    for page_num, page in _all_pages(book):
        if page.get("note"):
            yield f'{page_num}: {page.get("note")}'

        for photo in _page_photos(page):
            slot = photo.get("meta", "")
            if ", " in slot:
                slot = ", " + slot.split(",")[0]
            elif slot and photo.get("photo", "BLANK") == "BLANK":
                slot = f", {slot}"
            else:
                slot = ""

            if photo.get("note"):
                yield f'{page_num}{slot}: {photo.get("note")}'


def do_map_preview(
    console: Console,
    maps_file: Path,
    out: Path,
    *,
    launch: bool = False,
    config: Configuration,
) -> None:
    log_panel(console, f"Maps: {maps_file}", title=f"Generating map preview --> {out}")

    maps_model = read_maps(maps_file)

    with out.open("wt") as f:
        _map_preview(f, maps_model, config)

    if launch:
        console.log("Opening in browser...")
        click.launch(str(out))


def _map_preview(
    out: TextIO,
    maps_model: MapsModel,
    config: Configuration,
    map_width: int = 1268,
    map_height: int = 951,
) -> None:
    map_images = [
        get_map_image(map_model, map_width, map_height, config)
        for map_model in sorted(maps_model.maps, key=attrgetter("zoom"), reverse=True)
    ]

    for line in print_map_preview_html(maps_model, map_images, map_width, map_height):
        out.write(line)


def do_thumb_index(input_yaml: Path) -> None:
    source = read_yaml(input_yaml)

    index = {}

    index["book_title"] = source.get("book_title", "Untitled")
    if "book_spine" in source:
        index["book_spine"] = source.get("book_spine")

    index["params"] = {"gutter": 0.5}

    for mode in ["front", "in_front", "in_back", "back"]:
        if mode in source:
            index[mode] = source[mode]

    index["pages"] = get_thumbnail_pages(source)

    write_yaml(sys.stdout, index)
