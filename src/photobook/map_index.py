from collections import defaultdict
from collections.abc import Iterable
from dataclasses import dataclass
from datetime import datetime
from fractions import Fraction
from itertools import permutations
from operator import attrgetter, itemgetter
from pathlib import Path
from typing import Any

from book_index import (
    BookCalendar,
    Coord,
    IndexBuilder,
    MapImage,
    bounding_box,
    fetch_map,
)
from book_index.maps import MapIndex, Marker
from book_layout import BookCanvas, BookLayout, BookPage, book_pdf, photo_view
from book_layout.book_pdf import PhotoDetails
from book_layout.book_schema import (
    BookModel,
    LocationModel,
    MapModel,
    PageModel,
    UnknownLocations,
)
from h3 import h3
from label_layout import Port
from page_layout import Params, get_label, get_layout_by_id

from photobook.book_util import (
    PageDict,
    PhotoDict,
    _all_photos,
    _page_photos,
    even_odd,
    fix_photo_path,
    page_num2,
    read_yaml,
    write_yaml,
)
from photobook.cal_index import _get_date
from photobook.configuration import Configuration


@dataclass
class Location:
    name: str
    h3index: str | None = None  # collect photos at this hex
    cal_index: bool = False  # annotate photo with cal_caption for this location?
    h3index2: str | None = None  # collect labels at this hex

    @property
    def is_unknown(self) -> bool:
        return self == NOT_FOUND


NOT_FOUND = Location("*Not found*")

LocationIndex = dict[int, list[tuple[Location, str]]]
Annotations = dict[str, dict[str, str | None]]


def get_map_image(map_model: MapModel, width: int, height: int, config: Configuration) -> MapImage:
    center_lat, center_lon = map_model.get_center()
    top_left, bottom_right = bounding_box(center_lat, center_lon, map_model.zoom, width, height, scale=8)
    return MapImage(
        map_model.title,
        fetch_map(
            center_lon,
            center_lat,
            map_model.zoom,
            map_model.map,
            width,
            height,
            config.MAPBOX_KEY,
            config.map_path,
        ),
        Coord(top_left.lat, top_left.lon),
        Coord(bottom_right.lat, bottom_right.lon),
        width,
        height,
        map_model.h3resolution,
    )


def get_map_index(
    builder: IndexBuilder,
    label_width: float,
    marker_radius_cm: float = 0.0,
    marker_radius_px: float = 0.0,
) -> Iterable[PageDict]:
    map_slots = 3
    maps = add_map_insets(list(reversed(list(builder.included_maps()))))
    while maps:
        page_maps, maps = maps[:map_slots], maps[map_slots:]

        result: list[PhotoDict] = []
        for map_ in page_maps:
            # slug = Path(map_.image.filename).name.split('-')[0]
            result.append(
                {
                    "caption": map_.image.name,
                    "filename": map_.image.filename,
                    "bounds": f"{map_.image.top_left} - {map_.image.bottom_right}",
                    "lhs": [],
                    "rhs": [],
                }
            )
            if marker_radius_cm:
                result[-1]["marker_radius"] = marker_radius_cm
            if map_.insets:
                result[-1]["inset"] = map_.insets

            ports = get_best_map_layout(
                map_,
                builder.get_marker_caption,
                label_width=label_width,
                label_margin=0.2,  # cm
                marker_radius=marker_radius_px,
                scale_x=1.0,
                cm_to_px=marker_radius_px / marker_radius_cm,
            )

            for port in ports:
                result[-1]["rhs" if port.port_x > 0 else "lhs"].append(
                    {
                        "label": port.data.para_text,
                        "y": int(port.port_y),
                        "points": list({f"{marker.h3index} ({marker.x},{marker.y})" for marker in port.data.markers}),
                    }
                )

        yield {"page": result}


def get_best_map_layout(map_: MapIndex, *args: Any, **kwargs: Any) -> list[Port]:
    # try multiple versions of the layout and pick the "best" one
    # (based on shortest total line length)
    versions = [
        {
            # "divider_position": divider_position,
            "lhs_order": lhs_order,
            "rhs_order": rhs_order,
        }
        ## divider position just tends put all the markers
        ## on one side which isn't helpful
        # for divider_position in ('center', 'median')
        for lhs_order in ("top", "bottom")
        for rhs_order in ("top", "bottom")
    ]

    results = []
    for version in versions:
        leaders, ports = map_.get_label_layout(*args, **kwargs, **version)  # type: ignore
        results.append((ports, total_length(ports)))

    return min(results, key=itemgetter(1))[0]


def total_length(ports: list[Port]) -> float:
    def length(port: Port, marker: Marker) -> float:
        dx = port.port_x - marker.x
        dy = port.port_y - marker.y
        return dx * dx + dy * dy

    return sum(length(port, marker) for port in ports for marker in port.data.markers)


def get_map_index_builder(
    size: BookLayout,
    default_options: Params,
    maps: list[MapModel],
    cals: list[BookCalendar],
    config: Configuration,
) -> tuple[IndexBuilder, float, float, int]:
    pdf = BookCanvas(size)

    map_layout = get_layout_by_id("l3")

    page_params = pdf.book_layout.page(pdf.even_odd, default_options)
    page = BookPage(page_params, map_layout, default_options)
    map_slot = page.image_rect(map_layout.slots[1])
    map_rect = map_slot.pixel_rect(pdf.book_layout.cm_to_pixels)
    map_margin = map_slot.margin()
    label_margin = 0.2  # cm
    label_width = map_margin.left - page.config.safe_offset.left - label_margin - 0.5
    # convert marker radius in cm to pixels
    marker_radius_cm = 0.1
    marker_radius_px = pdf.book_layout.cm_to_pixels(marker_radius_cm)

    map_images = [
        get_map_image(map_model, map_rect.width, map_rect.height, config)
        for map_model in sorted(maps, key=attrgetter("zoom"), reverse=True)
    ]

    return (
        IndexBuilder(cals, map_images),
        label_width,
        marker_radius_cm,
        marker_radius_px,
    )


def _get_h3index(photo: PhotoDict, *, from_exif: bool = False) -> str | None:
    if "h3index" in photo:
        h3index = photo.get("h3index")
        if h3index is None:
            return None
        if not isinstance(h3index, str):
            raise TypeError(h3index)
        h3index = h3index.split(maxsplit=1)[0]
        if h3index == "None":
            return None

        return h3index

    if not from_exif:
        return None

    filename = photo.get("photo", "BLANK")
    if filename == "BLANK":
        return None

    details = PhotoDetails.load_photo(Path(filename))
    return details.h3index


def _get_exif_datetime(photo: PhotoDict) -> datetime | None:
    filename = photo.get("photo", "BLANK")
    if filename == "BLANK":
        return None

    details = PhotoDetails.load_photo(Path(filename))
    return photo_view.parse_exif_datetime(details.exif_datetime)


def get_location_index(locations: list[LocationModel]) -> dict[int, Any]:
    # process locations so they can be scanned in order from
    # smallest to biggest hexes: sorted(h3resolution, reverse=True)
    # dict: resolution -> list of (hex, location)
    location_index = defaultdict(list)
    for location in locations:
        # collect photos?
        # true (default): collect photos in this location in a single
        # point (at the first hex)
        # false: show photos at their individual locations
        h3index2 = location.hexes[0].split(",")[0]

        loc = Location(
            location.location,
            h3index2 if location.collect else None,
            location.cal_index,
            h3index2,
        )

        for hex_ in location.hexes:
            h3index, resolution = hex_.split(",")
            hex_h3index = h3.h3_to_parent(h3index, int(resolution))
            location_index[int(resolution)].append((loc, hex_h3index))

    return dict(sorted(location_index.items(), key=itemgetter(0), reverse=True))


def _lookup_location(location_index: LocationIndex, photo_h3index: str) -> tuple[Location, str]:
    for resolution, hexes in location_index.items():
        h3index = h3.h3_to_parent(photo_h3index, resolution)
        for location, location_h3index in hexes:
            if location_h3index == h3index:
                return location, (location.h3index or photo_h3index)

    return NOT_FOUND, photo_h3index


def build_map_index(
    builder: IndexBuilder,
    all_pages: Iterable[tuple[str, PageDict]],
    location_index: LocationIndex,
    book_model: BookModel,
    photo_dict: dict[str, PhotoDetails],
) -> None:
    for page_num, page in all_pages:
        page_model = PageModel(**page)
        try:
            book_page, content_layout, page_aspect = book_pdf.dummy_page(book_model, page_model, photo_dict)
        except book_pdf.InvalidLayoutError:
            # TODO: log out error?
            continue
        layout_params = book_page.params.layout_params(Fraction(1))
        ncols = len(content_layout.get_column_widths(layout_params, even_odd(page_num)))

        captions = [
            get_label(book_page.params.caption_position, caption, ncols).lower()
            for _photo_model, _image_rect, caption in book_pdf.get_photo_slots(
                book_page, page_model.photos, content_layout.slots
            )
        ]
        for photo, slot_caption in zip(_page_photos(page), captions, strict=False):
            year_month, day, hour = _get_date(photo)
            # cal_caption from photo, fall back to page, but if page is None
            # then we don't want to show a caption at all
            cal_caption = photo.get("cal_caption", "")
            if cal_caption == "" or page_model.cal_caption is None:
                cal_caption = page_model.cal_caption
            builder.add_photo1(
                year_month,
                day,
                hour,
                page_num2(page_num),
                slot_caption,
                cal_caption=cal_caption,
            )

            # just get h3index out of yaml - must have been annotated already
            # by lookup_photo_locations.  still need to lookup the location
            # to find the actual h3index to use however in case collect=True
            h3index = _get_h3index(photo, from_exif=False)
            if h3index is not None:
                location, h3index2 = _lookup_location(location_index, h3index)
                builder.add_photo2(
                    h3index2,
                    page_num2(page_num),
                    slot_caption,
                    location.name,
                    location_h3index=location.h3index2 or h3index2,
                )


def lookup_photo_datetimes(to_annotate: Annotations, photos: Iterable[PhotoDict]) -> None:
    for photo in photos:
        if dt := _get_exif_datetime(photo):
            to_annotate[photo["photo"]]["timestamp"] = str(dt)


def lookup_photo_locations(
    to_annotate: Annotations,
    photos: Iterable[PhotoDict],
    location_index: LocationIndex,
) -> UnknownLocations:
    # list of unknown locations
    unknown_locations = UnknownLocations()
    last_known: Location | None = None
    last_unknown: LocationModel | None = None
    for photo in photos:
        h3index = _get_h3index(photo, from_exif=True)
        if h3index is not None:
            location, h3index2 = _lookup_location(location_index, h3index)
            to_annotate[photo["photo"]]["h3index"] = f'{h3index} "{location.name}"'
            if location.cal_index and "cal_caption" not in photo:
                to_annotate[photo["photo"]]["cal_caption"] = location.name
            if location.is_unknown:
                last_unknown = unknown_locations.add_from_h3index(h3index)
                last_unknown.update_dates(_get_exif_datetime(photo))
                if last_known:
                    last_unknown.update_nearby(last_known.name)
                    last_known = None
            else:
                last_known = location
                if last_unknown:
                    last_unknown.update_nearby(last_known.name)
                    last_unknown = None
        else:
            to_annotate[photo["photo"]]["h3index"] = None

    return unknown_locations


def annotate_h3index(input_yaml: list[Path], to_annotate: Annotations) -> None:
    for input_yaml_ in input_yaml:
        book = read_yaml(input_yaml_)
        book_path = Path(input_yaml_).parent

        for photo in _all_photos(book):
            filename = fix_photo_path(photo["photo"], book_path)

            for key, new_value in to_annotate.get(filename, {}).items():
                old_value = photo.get(key)
                if new_value not in (None, old_value):
                    photo[key] = new_value

        write_yaml(input_yaml_, book)


def add_map_insets(maps: list[MapIndex]) -> list[MapIndex]:
    min_width = 0.05  # 5% of the map size
    for map1, map2 in permutations(maps, 2):
        map2_rect = map1.image.coord_rect_to_pix(map2.image.top_left, map2.image.bottom_right)
        if (
            map1.image.coords_on_map(map2.image.top_left, map2.image.bottom_right)
            and (map2_rect.width / map1.image.width) > min_width
        ):
            map1.insets.append(f"{map2.image.top_left} - {map2.image.bottom_right} {map2.image.name}")
    return maps
