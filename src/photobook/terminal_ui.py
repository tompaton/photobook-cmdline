from collections.abc import Iterable
from typing import Any

import stackprinter
from rich.console import Console, Group
from rich.panel import Panel
from rich.prompt import Confirm
from rich.status import Status
from rich.theme import Theme
from rich.tree import Tree

from photobook.reconcilers import Reconciler

theme = Theme({"info": "dim cyan", "warning": "magenta", "danger": "bold red"})


def create_console() -> Console:
    stackprinter.set_excepthook(style="darkbg2")

    return Console(theme=theme, log_path=False)


def log_panel(console: Console, contents: Any, title: str | None = None, style: str = "none") -> None:
    if title is not None and style == "none":
        title = f"[white]{title}[/white]"
    console.log(
        Panel(
            contents,
            title=title,
            title_align="left",
            border_style="bright_black" if style == "none" else style,
            style=style,
        )
    )


def title_panel(console: Console, reconciler_list: list[Reconciler]) -> None:
    log_panel(
        console,
        Group(*reconciler_tree(reconciler_list)),
        title="[bright_blue]Photobook Generator[/bright_blue]",
    )


def reconciler_tree(reconciler_list: list[Reconciler]) -> Iterable[Tree]:
    children: dict[str, list[tuple[str, str]]] = {}
    for reconciler in reconciler_list:
        for in_ in reconciler.input_files:
            children.setdefault(str(in_), [])
            for out in reconciler.output_files:
                children[str(in_)].append((reconciler.name, str(out)))

    def add(t: Tree, c: str) -> None:
        for dd, cc in children.pop(c, []):
            tt = t.add(f"{cc} [red]{dd}[/red]")
            add(tt, cc)

    roots = set(children.keys())
    for c in children:
        for _dd, cc in children[c]:
            roots.discard(cc)

    for r in sorted(roots, key=lambda r: "map" in r):
        t = Tree(label=r)
        add(t, r)
        yield t


def retry_error(console: Console, e: Exception) -> bool:
    # first line of exception is the panel title
    # last line is a link to pydantic docs, don't display that
    ee = str(e).split("\n")
    log_panel(console, "\n".join(ee[1:-1]), title=ee[0], style="danger")

    return Confirm.ask("            Retry? (fix first...)", default=True)


def new_book_details(console: Console, status: Status) -> tuple[str, bool]:
    status.stop()
    book_title = console.input("             :book: [bold red]Book title[/]? ")
    auto_captions = Confirm.ask("                Generate captions from photo dates?", default=False)
    status.start()
    return book_title, auto_captions
