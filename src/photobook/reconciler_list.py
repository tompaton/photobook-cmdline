from collections.abc import Iterable
from pathlib import Path

import rich_click as click
from book_layout.book_pdf import RenderMode

from photobook import reconcilers
from photobook.configuration import Configuration


def make_reconciler_list(
    maps: Path | None,
    photo_path: list[Path],
    *,
    final: bool = False,
    debug: bool = False,
    launch: bool = False,
    no_meta: bool = False,
    config: Configuration,
) -> Iterable[reconcilers.Reconciler]:
    # TODO: create backups automatically (particularly before overwriting files
    # that may get clobbered, and not just annotated)

    if len(photo_path) == 1:
        if maps is None:
            return make_one_book_no_maps(
                photo_path[0],
                final=final,
                debug=debug,
                launch=launch,
                no_meta=no_meta,
            )

        return make_one_book(
            maps,
            photo_path[0],
            final=final,
            debug=debug,
            launch=launch,
            no_meta=no_meta,
            config=config,
        )

    if maps is None:
        raise click.BadParameter(
            param_hint="--maps",
            message="Missing option '--maps' when creating volume.",
        )

    return make_volume(
        maps,
        photo_path,
        final=final,
        debug=debug,
        launch=launch,
        config=config,
    )


# ignore final for now for a single book
def make_one_book(
    map_yml: Path,
    photo_path: Path,
    *,
    final: bool = False,
    debug: bool = False,
    launch: bool = False,
    no_meta: bool = False,
    config: Configuration,
) -> Iterable[reconcilers.Reconciler]:
    yield reconcilers.MapPreview(
        map_yml,
        photo_path.parent / "map-preview.html",
        launch=launch,
        config=config,
    )

    if (photo_path.exists() and photo_path.is_file()) or (
        not photo_path.exists() and photo_path.suffix.lower() in (".yml", ".yaml")
    ):
        book_yaml = photo_path
    else:
        generate = reconcilers.GenerateBook(photo_path)
        yield generate
        book_yaml = generate.output_file

    annotate = reconcilers.Annotate(map_yml, book_yaml)
    yield annotate

    render_mode = RenderMode(debug=debug, final=final)
    render_mode.annotate = not no_meta

    yield reconcilers.Render(
        "Render Book",
        render_mode,
        book_yaml,
        launch=launch,
        unknown_yml=annotate.output_file,
    )

    index = reconcilers.GenerateIndex(map_yml, book_yaml, config=config)
    yield index

    yield reconcilers.Render(
        "Render Index",
        render_mode.just_pages(),
        index.output_file,
        launch=launch,
        no_warning=True,
    )


def make_one_book_no_maps(
    photo_path: Path,
    *,
    final: bool = False,
    debug: bool = False,
    launch: bool = False,
    no_meta: bool = False,
) -> Iterable[reconcilers.Reconciler]:
    if (photo_path.exists() and photo_path.is_file()) or (
        not photo_path.exists() and photo_path.suffix.lower() in (".yml", ".yaml")
    ):
        book_yaml = photo_path
    else:
        generate = reconcilers.GenerateBook(photo_path)
        yield generate
        book_yaml = generate.output_file

    render_mode = RenderMode(debug=debug, final=final)
    render_mode.annotate = not no_meta

    yield reconcilers.Render(
        "Render Book",
        render_mode,
        book_yaml,
        launch=launch,
    )


def make_volume(
    map_yml: Path,
    photo_paths: list[Path],
    *,
    final: bool = False,
    debug: bool = False,
    launch: bool = False,
    config: Configuration,
) -> Iterable[reconcilers.Reconciler]:
    # books will have been generated one at a time, we just need to get the
    # .yml file names
    book_yml = [reconcilers.GenerateBook(photo_path).output_file for photo_path in photo_paths]

    index = reconcilers.GenerateIndex(map_yml, *book_yml, insert_blanks=True, config=config)
    yield reconcilers.KeepManualEdits(index.output_file.with_suffix(f"{index.output_file.suffix}.generated"), index)

    render_mode = RenderMode(debug=debug, final=final)
    if render_mode.final:
        yield reconcilers.Render(
            "Render Pages",
            render_mode.just_pages(),
            *book_yml,
            index.output_file,
            launch=launch,
        )
        yield reconcilers.Render(
            "Render Cover",
            render_mode.just_cover(),
            *book_yml,
            index.output_file,
            launch=launch,
        )
    else:
        yield reconcilers.Render("Render Book", render_mode, *book_yml, index.output_file, launch=launch)
