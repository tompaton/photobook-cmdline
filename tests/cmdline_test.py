from pathlib import Path
from unittest.mock import ANY, MagicMock, Mock, call, patch

import pydantic
import pytest
import rich_click as click

from photobook import cmdline, terminal_ui


def test_make() -> None:
    mock_reconciler1 = Mock(input_files=[Path("in1.yml")], output_files=[Path("out1.yml")])
    mock_reconciler1.name = "Reconciler1"
    mock_reconciler2 = Mock(input_files=[Path("out1.yml")], output_files=[Path("out2.yml")])
    mock_reconciler2.name = "Reconciler2"
    run_once = Mock(side_effect=[True, False])
    mock_maps = Mock(name="mock-maps.yml")
    mock_book = Mock(name="mock-book/")
    with patch.object(
        cmdline,
        "make_reconciler_list",
        return_value=[mock_reconciler1, mock_reconciler2],
    ) as mock_make_reconciler_list:
        cmdline.console.record = True
        cmdline.console._log_render.show_time = False
        assert callable(cmdline.make.callback)
        cmdline.make.callback(mock_maps, [mock_book], keep_looping=run_once)

    assert mock_make_reconciler_list.mock_calls == [
        call(
            mock_maps,
            [mock_book],
            final=False,
            debug=False,
            launch=False,
            no_meta=False,
            config=ANY,
        )
    ]
    assert mock_reconciler1.mock_calls == [
        call.can_update(),
        call.update(cmdline.console, ANY),
    ]
    assert mock_reconciler2.mock_calls == []

    assert cmdline.console.export_text() == (
        "╭─ Photobook Generator ────────────────────────────────────────────────────────╮\n"
        "│ in1.yml                                                                      │\n"
        "│ └── out1.yml Reconciler1                                                     │\n"
        "│     └── out2.yml Reconciler2                                                 │\n"
        "╰──────────────────────────────────────────────────────────────────────────────╯\n"
    )


def test_make_noop() -> None:
    mock_reconciler1 = Mock(input_files=[Path("in1.yml")], output_files=[Path("out1.yml")])
    mock_reconciler1.can_update.return_value = False
    mock_reconciler2 = Mock(input_files=[Path("out1.yml")], output_files=[Path("out2.yml")])
    mock_reconciler2.can_update.return_value = False
    run_once = Mock(side_effect=[True, False])
    mock_maps = Mock(name="mock-maps.yml")
    mock_book = Mock(name="mock-book/")
    with (
        patch.object(
            cmdline,
            "make_reconciler_list",
            return_value=[mock_reconciler1, mock_reconciler2],
        ),
        patch.object(cmdline, "console") as mock_console,
    ):
        assert callable(cmdline.make.callback)
        cmdline.make.callback(mock_maps, [mock_book], keep_looping=run_once)

    assert mock_reconciler1.mock_calls == [
        call.can_update(),
    ]
    assert mock_reconciler2.mock_calls == [
        call.can_update(),
    ]

    assert mock_console.mock_calls == [
        call.log(ANY),
        call.status("[bold green] Updating..."),
        call.status().stop(),
    ]


def test_make_error() -> None:
    mock_reconciler1 = Mock(input_files=[Path("in1.yml")], output_files=[Path("out1.yml")])
    mock_reconciler1.update.side_effect = pydantic.ValidationError.from_exception_data("blah", [])
    mock_reconciler2 = Mock(input_files=[Path("out1.yml")], output_files=[Path("out2.yml")])
    run_once = Mock(side_effect=[True, False])
    mock_maps = Mock(name="mock-maps.yml")
    mock_book = Mock(name="mock-book/")
    with (
        patch.object(
            cmdline,
            "make_reconciler_list",
            return_value=[mock_reconciler1, mock_reconciler2],
        ),
        patch.object(cmdline, "console") as mock_console,
        patch.object(terminal_ui, "Confirm") as mock_confirm,
    ):
        mock_confirm.ask.return_value = False
        assert callable(cmdline.make.callback)
        cmdline.make.callback(mock_maps, [mock_book], keep_looping=run_once)

    assert mock_reconciler1.mock_calls == [
        call.can_update(),
        call.update(mock_console, mock_console.status.return_value),
    ]
    assert mock_reconciler2.mock_calls == []

    assert mock_console.mock_calls == [
        call.log(ANY),
        call.status("[bold green] Updating..."),
        call.status().start(),
        call.status().stop(),
        call.log(ANY),
    ]

    assert mock_confirm.mock_calls == [
        call.ask("            Retry? (fix first...)", default=True),
    ]


def test_make_visualize() -> None:
    mock_maps = Path("mock-maps.yml")
    mock_book = Path("mock-book/")
    assert callable(cmdline.make.callback)
    cmdline.make.callback(mock_maps, [mock_book], visualize=True)


def test_make_no_maps() -> None:
    mock_maps = MagicMock(name="mock-maps.yml")
    mock_maps.exists.return_value = False
    mock_maps.__str__.return_value = "mock-maps.yml"  # type: ignore
    mock_book = MagicMock(name="mock-book/")
    mock_book.__str__.return_value = "mock-book/"  # type: ignore
    assert callable(cmdline.make.callback)
    with patch.object(cmdline, "make_reconciler_list", return_value=[]), pytest.raises(click.BadParameter) as ex:
        cmdline.make.callback(mock_maps, [mock_book])
    assert str(ex.value) == "File 'mock-maps.yml' does not exist."


def test_make_no_books() -> None:
    mock_maps = MagicMock(name="mock-maps.yml")
    mock_maps.exists.return_value = True
    mock_maps.__str__.return_value = "mock-maps.yml"  # type: ignore
    mock_book = MagicMock(name="mock-book/")
    mock_book.exists.return_value = False
    mock_book.__str__.return_value = "mock-book/"  # type: ignore
    assert callable(cmdline.make.callback)
    with patch.object(cmdline, "make_reconciler_list", return_value=[]), pytest.raises(click.BadParameter) as ex:
        cmdline.make.callback(mock_maps, [mock_book])
    assert str(ex.value) == "Directory 'mock-book/' does not exist."
