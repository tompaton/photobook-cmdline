from book_index import Coord, MapImage
from book_index.maps import MapIndex

from photobook.map_index import add_map_insets


def test_add_map_insets() -> None:
    map1 = MapIndex(
        MapImage(
            "Victoria",
            "",
            Coord(-34.09643350385901, 141.12511610945668),
            Coord(-39.08128136244812, 149.40833851937555),
            1268,
            952,
            10,
        )
    )
    map2 = MapIndex(
        MapImage(
            "Great Ocean Road",
            "",
            Coord(-38.29236177807541, 143.56246948242185),
            Coord(-38.80306228724325, 144.4331359863281),
            1268,
            952,
            10,
        )
    )
    map3 = MapIndex(
        MapImage(
            "Melbourne",
            "",
            Coord(-37.65708633221547, 144.6216358567523),
            Coord(-38.021736902435286, 145.23729004581642),
            1268,
            952,
            10,
        )
    )
    map4 = MapIndex(
        MapImage(
            "Anglesea",
            "",
            Coord(-38.39900626030608, 144.1523838043213),
            Coord(-38.43098410688405, 144.2068004608154),
            1268,
            952,
            10,
        )
    )

    assert add_map_insets([map1, map2, map3, map4]) == [map1, map2, map3, map4]
    assert map1.insets == [
        "(-38.29236177807541, 143.56246948242185) - (-38.80306228724325, 144.4331359863281) Great Ocean Road",
        "(-37.65708633221547, 144.6216358567523) - (-38.021736902435286, 145.23729004581642) Melbourne",
    ]
    assert map2.insets == ["(-38.39900626030608, 144.1523838043213) - (-38.43098410688405, 144.2068004608154) Anglesea"]
    assert map3.insets == []
    assert map4.insets == []
