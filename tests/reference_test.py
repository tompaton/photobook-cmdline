import io
import re
import subprocess
import tempfile
from collections.abc import Iterator
from contextlib import contextmanager, redirect_stdout
from pathlib import Path
from unittest.mock import Mock

import pytest
import rich_click as click
from book_layout import photo_view
from book_layout.book_pdf import RenderMode
from book_layout.book_schema import CropDefinition
from page_layout import get_aspect

from photobook import cmdline, operations, reconciler_list, reference
from photobook.configuration import Configuration


def test_reference_cli() -> None:
    # compare the generated reference against the (possibly staged) version
    # committed in git.
    expected = read_staged_or_committed_file("docs/Command Line Reference.md")
    with redirect_stdout(io.StringIO()) as f:
        reference.print_reference_cli(cmdline.cli)
    result = f.getvalue()
    assert result == expected


def test_reference_schema() -> None:
    # compare the generated reference against the (possibly staged) version
    # committed in git.
    expected = read_staged_or_committed_file("docs/Photobook File Formats.md")
    with redirect_stdout(io.StringIO()) as f:
        reference.print_reference_schema()
    result = f.getvalue()
    assert result == expected


def test_reference_aspects_svg() -> None:
    expected = read_staged_or_committed_file("docs/images/reference-aspect.svg")
    result = reference.generate_reference_aspects()[1] + "\n"
    assert result == expected


def test_reference_layouts_svg() -> None:
    for _layout_table, svg_name, layout_svg in reference.generate_reference_layouts():
        expected = read_staged_or_committed_file(f"docs/{svg_name}")
        assert layout_svg == expected


def test_make_visualize_one_book() -> None:
    reconcilers = list(
        reconciler_list.make_reconciler_list(
            Path("books/map.yml"),
            [Path("books/YYYY-mm")],
            final=False,
            debug=False,
            launch=False,
            config=Configuration(),
        )
    )

    result = dot_to_svg(reference.render_reconciler_graph(reconcilers))
    expected = read_staged_or_committed_file("docs/images/make-one-book.svg")

    assert result == expected


def test_make_visualize_one_book_no_maps() -> None:
    reconcilers = list(
        reconciler_list.make_reconciler_list(
            None,
            [Path("books/YYYY-mm")],
            final=False,
            debug=False,
            launch=False,
            config=Configuration(),
        )
    )

    result = dot_to_svg(reference.render_reconciler_graph(reconcilers))
    expected = read_staged_or_committed_file("docs/images/make-one-book-no-maps.svg")

    assert result == expected


def test_make_visualize_volume_no_maps() -> None:
    with pytest.raises(click.BadParameter):
        list(
            reconciler_list.make_reconciler_list(
                None,
                [
                    Path("books/YYYY-mm1"),
                    Path("books/YYYY-mm2"),
                    Path("books/YYYY-mm3"),
                ],
                final=False,
                debug=False,
                launch=False,
                config=Configuration(),
            )
        )


def test_make_visualize_volume() -> None:
    reconcilers = list(
        reconciler_list.make_reconciler_list(
            Path("books/map.yml"),
            [Path("books/YYYY-mm1"), Path("books/YYYY-mm2"), Path("books/YYYY-mm3")],
            final=False,
            debug=False,
            launch=False,
            config=Configuration(),
        )
    )

    result = dot_to_svg(reference.render_reconciler_graph(reconcilers))
    expected = read_staged_or_committed_file("docs/images/make-combined-volume.svg")

    assert result == expected


def test_make_visualize_custom_book() -> None:
    reconcilers = list(
        reconciler_list.make_reconciler_list(
            Path("books/map.yml"),
            [Path("books/YYYY-mm-custom.yml")],
            final=False,
            debug=False,
            launch=False,
            config=Configuration(),
        )
    )

    result = dot_to_svg(reference.render_reconciler_graph(reconcilers))
    expected = read_staged_or_committed_file("docs/images/make-custom-book.svg")

    assert result == expected


def test_make_visualize_custom_book_no_maps() -> None:
    reconcilers = list(
        reconciler_list.make_reconciler_list(
            None,
            [Path("books/YYYY-mm-index.yml")],
            final=False,
            debug=False,
            launch=False,
            config=Configuration(),
        )
    )

    result = dot_to_svg(reference.render_reconciler_graph(reconcilers))
    expected = read_staged_or_committed_file("docs/images/make-custom-book-no-maps.svg")

    assert result == expected


def dot_to_svg(svg: str) -> str:
    # NOTE: assumes dot is installed...
    return subprocess.run(["dot", "-Tsvg"], text=True, input=svg, capture_output=True, check=True).stdout


def read_staged_or_committed_file(filename: str) -> str:
    with read_staged_or_committed_file2(filename) as fn:
        return fn.read_text()


@contextmanager
def read_staged_or_committed_file2(filename: str) -> Iterator[Path]:
    with tempfile.TemporaryDirectory(prefix="test") as tmpdir:
        fn: Path = Path(tmpdir) / filename
        fn.parent.mkdir(parents=True, exist_ok=True)
        subprocess.check_call(["git", "checkout-index", "--prefix", f"{tmpdir}/", "--", filename])
        yield fn


def test_render_example_map() -> None:
    """render a sample map yaml, convert the temp pdf to svg, write to docs/
    directory and compare to committed version"""
    operations.configure()

    console = Mock()
    example_yml = Path("docs/examples/map-index.yml")
    example_svg = Path("docs/images/map-index.svg")
    render_mode = RenderMode(final=True)
    render_mode.annotate = False
    with tempfile.NamedTemporaryFile(suffix=".pdf") as temp_pdf, tempfile.NamedTemporaryFile(suffix=".svg") as temp_svg:
        # photobook render docs/examples/map-index.yml --no-cover --final \
        # --no-annotate --out /tmp/map-index.pdf
        operations.do_render(
            console,
            [example_yml],
            render_mode,
            out=Path(temp_pdf.name),
            title="",
            spine="",
            launch=False,
        )

        # pdf2svg /tmp/map-index.pdf docs/images/map-index.svg 1
        subprocess.check_call(["pdf2svg", temp_pdf.name, temp_svg.name])

        result = Path(temp_svg.name).read_text()

        # crop to map by modifing viewBox
        result = result.replace(
            '<svg xmlns="http://www.w3.org/2000/svg" '
            'xmlns:xlink="http://www.w3.org/1999/xlink" '
            'width="620.9858" height="809.9717" '
            'viewBox="0 0 620.9858 809.9717">',
            '<svg xmlns="http://www.w3.org/2000/svg" '
            'xmlns:xlink="http://www.w3.org/1999/xlink" '
            'width="980" height="500" '
            'viewBox="50 80 540 250">',
            1,
        )

        example_svg.write_text(result)

    # compare to staged/committed versions
    expected = read_staged_or_committed_file(str(example_svg))
    assert result == expected


def test_render_test_book() -> None:
    """render test-book.yml and write pdf to docs/ directory (adjusting
    timestamps) and compare to committed version"""
    operations.configure()

    console = Mock()
    test_yml = Path("docs/examples/test-book.yml")
    test_pdf = Path("docs/examples/test-book.pdf")
    render_mode = RenderMode(debug=True)
    render_mode.annotate = False
    render_mode.cover = False

    # clear out resampled_image output cache
    for f in (test_yml.parent / "output").glob("*"):
        f.unlink()

    with tempfile.NamedTemporaryFile(suffix=".pdf") as temp_pdf:
        # photobook render docs/examples/test-book.yml --debug --no-cover
        # --no-annotate --out /tmp/temp.pdf
        operations.do_render(
            console,
            [test_yml],
            render_mode,
            out=Path(temp_pdf.name),
            title="",
            spine="",
            launch=False,
        )

        result = Path(temp_pdf.name.replace(".pdf", " (pages).pdf")).read_bytes()

        # replace timestamps
        result = re.sub(
            b"/CreationDate \\(D:\\d{14}-\\d\\d'\\d\\d'\\) ",
            b"/CreationDate (D:20240513213632-10'00') ",
            result,
            count=1,
        )
        result = re.sub(
            b"/ModDate \\(D:\\d{14}-\\d\\d'\\d\\d'\\) ",
            b"/ModDate (D:20240513213632-10'00') ",
            result,
            count=1,
        )

        result = re.sub(
            b"/ID \\n\\[<[0-9a-f]{32}><[0-9a-f]{32}>\\]\\n",
            b"/ID \\n[<3b8c197476d162b3c1b31c9ae88eb2b0><3b8c197476d162b3c1b31c9ae88eb2b0>]\\n",
            result,
            count=1,
        )

        test_pdf.write_bytes(result)

    # compare to staged/committed versions
    with read_staged_or_committed_file2(str(test_pdf)) as fn:
        expected = fn.read_bytes()

    assert result == expected


# this isn't really reference, but given we're generating svgs and comparing
# them against reference images in docs/images/ it's more appropriate than
# in book-layout


@pytest.mark.parametrize(
    (
        "caption",
        "test_svg",
        "src_width",
        "src_height",
        "crop",
        "dst_width",
        "dst_height",
        "aspect",
    ),
    [
        (
            "Landscape in a landscape slot",
            Path("docs/images/resample_image-1.svg"),
            4000,
            3000,
            "outside landscape",
            800,
            600,
            "6x8",
        ),
        (
            "Portrait in a landscape slot",
            Path("docs/images/resample_image-2.svg"),
            3000,
            4000,
            "outside landscape",
            800,
            600,
            "6x8",
        ),
        (
            "Crop landscape out of center of portrait image",
            Path("docs/images/resample_image-3.svg"),
            3000,
            4000,
            "inside landscape",
            800,
            600,
            "6x8",
        ),
        (
            "Crop landscape out of panorama",
            Path("docs/images/resample_image-4.svg"),
            4000,
            2000,
            "inside landscape left",
            800,
            600,
            "6x8",
        ),
        (
            "Mismatched aspect, outside crop",
            Path("docs/images/resample_image-5.svg"),
            1600,
            900,
            "outside landscape",
            800,
            600,
            "6x8",
        ),
        (
            "Mismatched aspect, inside crop",
            Path("docs/images/resample_image-6.svg"),
            1600,
            900,
            "inside landscape",
            800,
            600,
            "6x8",
        ),
        (
            "Extended landscape crop from panorama",
            Path("docs/images/resample_image-7.svg"),
            4000,
            2000,
            "inside landscape left",
            1000,
            563,
            "9x16",
        ),
        (
            "Extended slot",
            Path("docs/images/resample_image-8.svg"),
            4000,
            3000,
            "inside landscape left",
            1000,
            563,
            "9x16",
        ),
        (
            "photo_view_test.py::test_resampled_image_crop1",
            Path("docs/images/resample_image-9.svg"),
            300,
            600,
            "outside portrait",
            100,
            178,
            "9x16",
        ),
    ],
)
def test_photo_view_resample_image_svg(
    caption: str,
    test_svg: Path,
    src_width: int,
    src_height: int,
    crop: str,
    dst_width: int,
    dst_height: int,
    aspect: str,
) -> None:
    aspect_ = get_aspect(aspect)
    # calculate resample parameters
    slot_window = photo_view.SlotWindow.from_pan_crop(*CropDefinition(crop).parse_crop(src_width, src_height, aspect_))
    src = photo_view.ImageSrc.from_slot_window(src_width, src_height, slot_window)
    dst = photo_view.ImageDst.from_slot_window(dst_width, dst_height, src, slot_window)

    # make sure we use actual values from src/dst objects
    del src_width
    del src_height
    del dst_width
    del dst_height

    # generate svg
    svg = reference.SVG(src.maxx, src.maxy)

    with svg.document(width=1000, viewbox=(0, 0, 200, svg.yy(src.maxy) + svg.pad)):
        svg.rect(
            0,
            0,
            svg.xx(src.maxx) + svg.pad,
            svg.yy(src.maxy) + svg.pad,
            "silver",
            stroke_width=0.1,
        )
        svg.text(
            3,
            7,
            text_anchor="start",
            content=caption,
            style="",
            font_family="sans-serif",
            font_size="5px",
        )
        svg.text(3, 12, fill="green", text_anchor="start", content=crop)

        svg.rect(
            svg.xx(dst.right - dst.width),
            svg.yy(dst.bottom - dst.height),
            svg.xx(dst.width) - svg.xx(0),
            svg.yy(dst.height) - svg.yy(0),
            "magenta",
            stroke_dasharray=2,
        )
        svg.text(
            svg.xx(dst.right) - 3,
            svg.yy(dst.bottom - dst.height) + 5,
            text_anchor="end",
            fill="magenta",
            content=f"{dst.width}x{dst.height}",
        )

        # render "3D" layers
        # svg._write('<g transform="scale(.7)">')
        # svg._write('<g transform="skewX(10) rotate(10)">')
        svg.image_rect(
            "background",
            "red",
            "rgba(255,0,0,.1)",
            (0, 0, src.maxx, src.maxy),
            "left",
            origin=False,
            center=False,
        )
        svg.dimension(f"maxx = {src.maxx}", "top", "red", 0, src.maxx)
        svg.dimension(f"maxy = {src.maxy}", "left", "red", 0, src.maxy)
        # svg._write("</g>")

        if src.padx or src.pady:
            svg.arrow("blue", 0, 0, src.padx, src.pady)

        # svg._write('<g transform="translate(-5 1) skewX(10) rotate(10)">')
        svg.image_rect(
            "image",
            "blue",
            "rgba(0,0,255,.1)",
            (src.padx, src.pady, src.width, src.height),
            "center",
        )
        svg.text(
            svg.xx(src.padx + src.width) - 3,
            svg.yy(src.pady) + 5,
            text_anchor="end",
            fill="blue",
            content=f"{src.width}x{src.height}",
        )

        svg.text(
            svg.xx(src.padx) + 3,
            svg.yy(src.pady) + 5,
            text_anchor="start",
            content=f"pad = {src.padx},{src.pady}",
        )
        # svg._write("</g>")

        # svg._write('<g transform="translate(-10 2) skewX(10) rotate(10)">')
        svg.image_rect(
            "slot",
            "green",
            "rgba(0,128,0,.1)",
            (dst.left, dst.top, dst.right - dst.left, dst.bottom - dst.top),
            "right",
            origin=False,
        )
        svg.dimension(
            f"crop_y = {slot_window.crop_y}",
            "right",
            "green",
            dst.top,
            dst.bottom,
        )
        svg.dimension(
            f"crop_x = {slot_window.crop_x}",
            "bottom",
            "green",
            dst.left,
            dst.right,
        )

        x1, y1 = svg.center(src.padx, src.pady, src.width, src.height)
        x2, y2 = svg.center(dst.left, dst.top, dst.right - dst.left, dst.bottom - dst.top)
        if x1 != x2 or y1 != y2:
            svg.arrow("green", x1, y1, x2, y2)
            svg.text(
                svg.xx(x2),
                svg.yy(y2) + 5,
                content=f"pan = {slot_window.pan_x:.2f},{slot_window.pan_y:.2f}",
            )

        # svg._write("</g>")
        # svg._write("</g>")

    # write to docs/images
    test_svg.write_text(svg.result)
    # compare to staged/committed versions
    expected = read_staged_or_committed_file(str(test_svg))

    assert svg.result == expected
