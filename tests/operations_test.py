import sys
from collections.abc import Iterator
from copy import deepcopy
from fractions import Fraction
from pathlib import Path, PosixPath
from typing import TYPE_CHECKING
from unittest.mock import ANY, MagicMock, Mock, call, patch

import pytest
from book_layout import book_pdf, photo_view
from book_layout.book_canvas import BookCanvas
from book_layout.book_schema import CropDefinition, LocationModel, MapModel, MapsModel
from page_boxes import ImageBox, ImageRect, PdfBox
from page_layout import Caption

from photobook import book_util, map_index, map_preview, operations

if TYPE_CHECKING:
    from photobook.book_util import BookDict

from photobook.configuration import Configuration


def photo_list(*filenames: str) -> list[book_pdf.PhotoDetails]:
    photos = [
        book_pdf.PhotoDetails(
            filename="mock-book/PXL_20230803_073143580.jpg",
            exif_datetime="2023:08:03 17:31:43",
            width=4624,
            height=3472,
            orientation="landscape",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/IMG_0598.JPEG",
            exif_datetime="2023:08:04 07:11:34",
            width=1536,
            height=2048,
            orientation="portrait",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/RenderedImage.jpg",
            exif_datetime="2023:08:05 09:04:31",
            width=4032,
            height=3024,
            orientation="landscape",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/PXL_20230806_063440528.jpg",
            exif_datetime="2023:08:06 16:34:40",
            width=3472,
            height=4624,
            orientation="portrait",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/IMG_0605.JPEG",
            exif_datetime="2023:08:07 20:37:03",
            width=1536,
            height=2048,
            orientation="portrait",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/PXL_20230803_073143580_2.jpg",
            exif_datetime="2023:08:03 17:31:44",
            width=4624,
            height=3472,
            orientation="landscape",
            aspect="8x10",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/IMG_0598_2.JPEG",
            exif_datetime="2023:08:04 07:11:34",
            width=2048,
            height=1536,
            orientation="landscape",
            aspect="6x8",
        ),
        book_pdf.PhotoDetails(
            filename="mock-book/PXL_20230827_082242920.jpg",
            exif_datetime="2023:08:27 17:31:43",
            width=4624,
            height=3472,
            orientation="landscape",
            aspect="6x8",
        ),
    ]

    if filenames:
        return [photo for photo in photos if photo.filename in filenames]

    return photos


@pytest.fixture
def mock_load_photo() -> Iterator[Mock]:
    photos = photo_list()
    photo_details = {Path(photo.filename): photo for photo in photos}
    with patch.object(book_pdf.PhotoDetails, "load_photo") as mock_load_photo_:
        mock_load_photo_.side_effect = photo_details.get

        yield mock_load_photo_


def test_generate_book_empty() -> None:
    photos: list[book_pdf.PhotoDetails] = []
    assert book_pdf.generate_book(photos) == {
        "book_title": "Untitled",
        "params": {"gutter": 0.5},
    }


def test_do_generate_book() -> None:
    photos = sorted(
        photo_list(
            "mock-book/PXL_20230803_073143580.jpg",
            "mock-book/IMG_0598.JPEG",
            "mock-book/RenderedImage.jpg",
            "mock-book/PXL_20230806_063440528.jpg",
            "mock-book/IMG_0605.JPEG",
        )
    )

    expected_book = {
        "params": {"gutter": 0.5},
        "book_title": "Untitled",
        "book_spine": "Untitled",
        "in_front": {
            "page": [{"photo": "mock-book/PXL_20230803_073143580.jpg"}],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [{"photo": "mock-book/IMG_0598.JPEG"}],
                "caption": "Friday, 04 August 2023",
            },
            {
                "page": [{"photo": "mock-book/RenderedImage.jpg"}],
                "caption": "Saturday, 05 August 2023",
            },
            {
                "page": [{"photo": "mock-book/PXL_20230806_063440528.jpg"}],
                "caption": "Sunday, 06 August 2023",
            },
        ],
        "in_back": {
            "page": [{"photo": "mock-book/IMG_0605.JPEG"}],
            "caption": "Monday, 07 August 2023",
        },
    }

    annotated_book = {
        "params": {"gutter": 0.5},
        "book_title": "Untitled",
        "book_spine": "Untitled",
        "in_front": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580.jpg",
                    "meta": "landscape 4624x3472 6x8",
                }
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/RenderedImage.jpg",
                        "meta": "landscape 4032x3024 6x8",
                    }
                ],
                "caption": "Saturday, 05 August 2023",
                "meta": "page 2 (left)",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/PXL_20230806_063440528.jpg",
                        "meta": "portrait 3472x4624 6x8",
                    }
                ],
                "caption": "Sunday, 06 August 2023",
                "meta": "page 3 (right)",
            },
        ],
        "in_back": {
            "page": [
                {
                    "photo": "mock-book/IMG_0605.JPEG",
                    "meta": "portrait 1536x2048 6x8",
                }
            ],
            "caption": "Monday, 07 August 2023",
        },
        "warning": "blank front cover, back cover, last 1 pages.",
    }

    console = Mock()
    status = Mock()

    with (
        patch.object(book_pdf.PhotoDetails, "load_photos") as mock_load_photos,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(operations, "read_yaml") as mock_read_yaml,
        patch.object(operations, "new_book_details", return_value=("Untitled", True)) as mock_new_book_details,
    ):
        mock_load_photos.return_value = photos
        mock_read_yaml.return_value = deepcopy(expected_book)

        operations.do_generate_book(console, status, Path("mock-book/"), Path("mock-book.yml"))

    assert mock_load_photos.mock_calls == [call(Path("mock-book/"))]

    assert mock_write_yaml.mock_calls == [
        call(ANY, expected_book, close=False),
        call(
            PosixPath("mock-book.yml"),
            annotated_book,
            add_schema="# yaml-language-server: $schema=schema/book-schema.json\n\n",
        ),
    ]

    assert mock_read_yaml.mock_calls == [call(ANY)]
    assert mock_new_book_details.mock_calls == [call(console, status)]


def test_annotate_book_bad_aspect() -> None:
    book = {
        "params": {"gutter": 0.5},
        "book_title": "Untitled",
        "in_front": {
            "page": [
                {"photo": "mock-book/IMG_0598_2.JPEG"},
                {"photo": "mock-book/PXL_20230803_073143580_2.jpg"},
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {"photo": "mock-book/IMG_0598_2.JPEG"},
                    {"photo": "mock-book/PXL_20230803_073143580_2.jpg"},
                ],
                "caption": "Friday, 04 August 2023",
            },
        ],
    }

    photo_dict = {photo.filename: photo for photo in photo_list()}

    annotated_book = book_pdf.annotate(book, photo_dict)

    assert annotated_book == {
        "params": {"gutter": 0.5},
        "book_title": "Untitled",
        "in_front": {
            "page": [
                {
                    "photo": "mock-book/IMG_0598_2.JPEG",
                    "meta": "Top, landscape 2048x1536 6x8",
                },
                {
                    "photo": "mock-book/PXL_20230803_073143580_2.jpg",
                    "meta": "Bottom, landscape 4624x3472 8x10 **bad aspect**",
                },
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598_2.JPEG",
                        "meta": "Top, landscape 2048x1536 6x8",
                    },
                    {
                        "photo": "mock-book/PXL_20230803_073143580_2.jpg",
                        "meta": "Bottom, landscape 4624x3472 8x10 **bad aspect**",
                    },
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
            }
        ],
        "warning": "page 1: bad aspect.  page in_front: bad aspect.  blank front cover, inside back cover, back cover, last 3 pages.",
    }


@pytest.fixture
def mock_read_maps() -> Iterator[Mock]:
    maps = MapsModel(
        maps=[
            MapModel(
                map="vicTESTtoria",
                title="Victoria",
                center="-36.64 145.29",
                zoom=5.75,
                h3resolution=5,
            ),
            MapModel(
                map="melTESTbourne",
                title="Melbourne",
                center="-37.84 144.93",
                zoom=9.5,
                h3resolution=8,
            ),
        ],
        locations=[
            LocationModel(
                location="123 Home St",
                hexes=[
                    "8fbe63cdcaa3826,10",
                ],
            ),
        ],
    )
    with patch.object(operations, "read_maps", return_value=maps) as mock_read_maps_:
        yield mock_read_maps_
    assert mock_read_maps_.mock_calls == [call(Path("mock-map.yml"))]


def test_do_annotate(mock_read_maps: Mock, mock_load_photo: Mock) -> None:
    book_ = {
        "params": {"gutter": 0.5},
        "book_title": "2023 Test Book",
        "in_front": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cd1a6e34c "Gardiners creek"',
                }
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/RenderedImage.jpg",
                        "meta": "landscape 4032x30246x8",
                    }
                ],
                "caption": "Saturday, 05 August 2023",
                "meta": "page 2 (left)",
            },
        ],
        "in_back": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230827_082242920.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cdcaa3826 "123 Home St"',
                }
            ],
            "caption": "Sunday, 27 August 2023",
        },
        "back": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580_2.jpg",
                    "h3index": '8fbe63cd1a6e34c "*Not found*"',
                }
            ]
        },
        "warning": "blank front cover.",
        "titles": ["2023 Test Book"],
    }

    annotated_book = {
        "params": {"gutter": 0.5},
        "book_title": "2023 Test Book",
        "in_front": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cd1a6e34c "*Not found*"',
                }
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/RenderedImage.jpg",
                        "meta": "landscape 4032x30246x8",
                    }
                ],
                "caption": "Saturday, 05 August 2023",
                "meta": "page 2 (left)",
            },
        ],
        "in_back": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230827_082242920.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cdcaa3826 "123 Home St"',
                }
            ],
            "caption": "Sunday, 27 August 2023",
        },
        "back": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580_2.jpg",
                    "h3index": '8fbe63cd1a6e34c "*Not found*"',
                }
            ]
        },
        "warning": "blank front cover.",
        "titles": ["2023 Test Book"],
    }

    unknown_locations = {
        "locations": [
            {
                "location": "Unknown",
                "hexes": ["8fbe63cd1a6e34c,10"],
                "map_url": "https://www.google.com/maps/@-37.871063995104905,145.09652229328879,14z",
                "first_date": "2023-08-03 17:31:43",
                "last_date": "2023-08-03 17:31:44",
                "count": 2,
                "nearby": "123 Home St",
            },
        ]
    }

    console = Mock()
    unknown_path = Mock()

    with (
        patch.object(book_util, "read_yaml", return_value=book_) as mock_read_yaml,
        patch.object(map_index, "read_yaml", return_value=book_) as mock_read_yaml2,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(map_index, "write_yaml") as mock_write_yaml2,
    ):
        operations.do_annotate(
            console,
            [Path("mock-book.yml")],
            Path("mock-map.yml"),
            unknown=unknown_path,
        )

    assert mock_read_yaml.mock_calls == [call(Path("mock-book.yml"))]
    assert mock_read_yaml2.mock_calls == [call(Path("mock-book.yml"))]
    assert mock_write_yaml.mock_calls == [call(unknown_path, unknown_locations)]
    assert mock_write_yaml2.mock_calls == [call(Path("mock-book.yml"), annotated_book)]

    assert mock_load_photo.mock_calls == [
        call(Path("mock-book/PXL_20230803_073143580.jpg")),
        call(Path("mock-book/PXL_20230803_073143580_2.jpg")),
        call(Path("mock-book/RenderedImage.jpg")),
    ]


def test_do_generate_index(mock_read_maps: Mock, mock_load_photo: Mock) -> None:
    operations.configure()

    book_ = {
        "params": {"gutter": 0.5},
        "book_title": "2023 Test Book",
        "in_front": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230803_073143580.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cd1a6e34c "Gardiners creek"',
                }
            ],
            "caption": "Thursday, 03 August 2023",
        },
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/RenderedImage.jpg",
                        "meta": "landscape 4032x30246x8",
                    }
                ],
                "caption": "Saturday, 05 August 2023",
                "meta": "page 2 (left)",
            },
        ],
        "in_back": {
            "page": [
                {
                    "photo": "mock-book/PXL_20230827_082242920.jpg",
                    "meta": "landscape 4624x3472 6x8",
                    "h3index": '8fbe63cdcaa3826 "123 Home St"',
                }
            ],
            "caption": "Sunday, 27 August 2023",
        },
        "warning": "blank front cover, back cover.",
        "titles": ["2023 Test Book"],
    }

    console = Mock()
    with (
        patch.object(
            map_index,
            "fetch_map",
            return_value="maps/vicTESTtoria-111x222_deadbeef.png",
        ) as mock_fetch_map,
        patch.object(book_util, "read_yaml", return_value=book_) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
    ):
        operations.do_generate_index(
            console,
            [Path("mock-book.yml")],
            Path("mock-map.yml"),
            Path("mock-index.yml"),
            insert_blanks=True,
            config=Configuration(),
        )

    assert mock_fetch_map.mock_calls == [
        call(144.93, -37.84, 9.5, "melTESTbourne", 1268, 951, "", ANY),
        call(145.29, -36.64, 5.75, "vicTESTtoria", 1268, 951, "", ANY),
    ]
    assert mock_read_yaml.mock_calls == [call(Path("mock-book.yml"))]
    assert mock_load_photo.mock_calls == [
        call(Path("mock-book/PXL_20230803_073143580.jpg")),
        call(Path("mock-book/PXL_20230827_082242920.jpg")),
        call(Path("mock-book/IMG_0598.JPEG")),
        call(Path("mock-book/RenderedImage.jpg")),
        call(Path("mock-book/PXL_20230803_073143580.jpg")),
        call(Path("mock-book/PXL_20230827_082242920.jpg")),
        call(Path("mock-book/IMG_0598.JPEG")),
        call(Path("mock-book/RenderedImage.jpg")),
        call(Path("mock-book/PXL_20230803_073143580.jpg")),
        call(Path("mock-book/PXL_20230827_082242920.jpg")),
        call(Path("mock-book/IMG_0598.JPEG")),
        call(Path("mock-book/RenderedImage.jpg")),
    ]

    index = {
        "params": {"gutter": 0.5},
        "pages": [
            {
                "page": [
                    {
                        "caption": "Victoria",
                        "filename": "maps/vicTESTtoria-111x222_deadbeef.png",
                        "bounds": "(-34.09643350385901, 141.12511610945668) - (-39.08128136244812, 149.40833851937555)",
                        "lhs": [],
                        "rhs": [
                            {
                                "label": "*Not found*: inside front<br/>123 Home St: 1, inside back",
                                "y": 476,
                                "points": ["85be63cffffffff (606,726)"],
                            }
                        ],
                        "marker_radius": 0.1,
                    }
                ]
            },
            {
                "page": [
                    {
                        "caption": "August 2023",
                        "cal": {
                            1: "Tue",
                            2: "Wed",
                            3: "Thu inside front",
                            4: "Fri 1",
                            5: "Sat 2",
                            6: "Sun",
                            7: "Mon",
                            8: "Tue",
                            9: "Wed",
                            10: "Thu",
                            11: "Fri",
                            12: "Sat",
                            13: "Sun",
                            14: "Mon",
                            15: "Tue",
                            16: "Wed",
                            17: "Thu",
                            18: "Fri",
                            19: "Sat",
                            20: "Sun",
                            21: "Mon",
                            22: "Tue",
                            23: "Wed",
                            24: "Thu",
                            25: "Fri",
                            26: "Sat",
                            27: "Sun inside back",
                            28: "Mon",
                            29: "Tue",
                            30: "Wed",
                            31: "Thu",
                        },
                    }
                ]
            },
        ],
    }

    assert mock_write_yaml.mock_calls == [call(Path("mock-index.yml"), index)]


def test_do_map_preview(mock_read_maps: Mock) -> None:
    console = Mock()
    out = MagicMock()
    with patch.object(
        map_index,
        "fetch_map",
        side_effect=[
            "maps/melTESTbourne-111x222_1234abcd.png",
            "maps/vicTESTtoria-111x222_deadbeef.png",
        ],
    ) as mock_fetch_map:
        operations.do_map_preview(
            console,
            Path("mock-map.yml"),
            out,
            launch=True,
            config=Configuration(),
        )

    assert mock_fetch_map.mock_calls == [
        call(144.93, -37.84, 9.5, "melTESTbourne", 1268, 951, "", ANY),
        call(145.29, -36.64, 5.75, "vicTESTtoria", 1268, 951, "", ANY),
    ]

    assert out.open.return_value.__enter__.return_value.mock_calls == [
        call.write(map_preview.MAP_HTML_PRE),
        call.write("<h3>Issues</h3><ul>"),
        call.write("</ul>"),
        call.write("<h3>Melbourne</h3>"),
        call.write(
            '<div style="position: relative;">'
            '<img src="../maps/melTESTbourne-111x222_1234abcd.png" />'
            '<canvas id="map0" width=1268 height=951 '
            'style="position: absolute; top: 0; left: 0"></canvas>'
            '<script>hexes("map0", [[734,586,734,586,734,586,734,586,734,586,734,586]], '
            '[[735,588,736,586,734,584,733,585,733,587,734,588]], []); trees("map0", []);'
            "</script></div>"
        ),
        call.write("<h3>Victoria</h3>"),
        call.write(
            '<div style="position: relative;">'
            '<img src="../maps/vicTESTtoria-111x222_deadbeef.png" />'
            '<canvas id="map1" width=1268 height=951 '
            'style="position: absolute; top: 0; left: 0"></canvas>'
            '<script>hexes("map1", [[476,757,476,757,476,757,476,757,476,757,476,757]], '
            '[[476,757,476,757,476,757,476,757,476,757,476,757]], []); trees("map1", []);'
            "</script></div>"
        ),
        call.write(map_preview.MAP_HTML_POST),
    ]


def test_do_render(mock_load_photo: Mock) -> None:
    book_: BookDict = {
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                        "note": "photo note",
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
                "note": "page note",
            },
        ]
    }
    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=book_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        render_mode = book_pdf.RenderMode()
        operations.do_render(
            console,
            [Path("mock-book.yml")],
            render_mode,
            out=out,
            launch=True,
            title="Test Book",
            spine="Test Book spine",
            unknown_yml=Mock(),
        )

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-book.yml")),
    ]

    assert mock_write_yaml.mock_calls == [call(Path("mock-book.yml"), book_)]

    assert mock_resampled_image.mock_calls == [
        call(
            "mock-book/IMG_0598.JPEG",
            CropDefinition(),
            ImageRect(width=1715, height=2287),
            ImageBox(left=0, bottom=0, width=1715, height=2287),
            page_colour=(255, 255, 0),
        )
    ]
    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                left="4.008",
                bottom="5.353",
                width="14.526",
                height="19.368",
            ).annotate(Caption.Blank),
            mock_resampled_image.return_value,
        )
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]

    assert book_ == {
        "book_title": "Test Book",
        "book_spine": "Test Book spine",
        "warning": "blank front cover, inside front cover, inside back cover, back cover, last 3 pages.",
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                        "note": "photo note",
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
                "note": "page note",
            }
        ],
    }


def test_do_render_debug(mock_load_photo: Mock) -> None:
    book_: BookDict = {
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "123 Home St"',
                        "note": "photo note",
                    }
                ],
                "caption": "Friday, 04 August 2023",
                "meta": "page 1 (right)",
                "note": "page note",
            },
            {
                "page": [
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "h3index": '8fbe63cdcaa3826 "*Not found*"',
                        "caption": "Friday, 04 August 2023",
                    }
                ],
                "meta": "page 2 (left)",
            },
        ]
    }
    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=book_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        render_mode = book_pdf.RenderMode(debug=True)
        render_mode.annotate = False
        operations.do_render(console, [Path("mock-book.yml")], render_mode, out=out)

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-book.yml")),
    ]

    assert mock_write_yaml.mock_calls == []

    assert mock_resampled_image.mock_calls == [
        call(
            "mock-book/IMG_0598.JPEG",
            CropDefinition(),
            ImageRect(width=1715, height=2287),
            ImageBox(left=0, bottom=0, width=1715, height=2287),
            page_colour=(255, 255, 0),
        ),
        call(
            "mock-book/IMG_0598.JPEG",
            CropDefinition(),
            ImageRect(width=1715, height=2287),
            ImageBox(left=0, bottom=0, width=1715, height=2287),
            page_colour=(255, 255, 0),
        ),
    ]

    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                left="4.008",
                bottom="5.353",
                width="14.526",
                height="19.368",
            ).annotate(Caption.Blank),
            mock_resampled_image.return_value,
        ),
        call(
            PdfBox(
                left="3.373",
                bottom="5.353",
                width="14.526",
                height="19.368",
            ).annotate(Caption.Blank),
            mock_resampled_image.return_value,
        ),
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]


def test_do_render_spanned(mock_load_photo: Mock) -> None:
    book_: BookDict = {
        "pages": [
            {
                "layout": "l6",
                "page": [
                    {
                        "photo": "mock-book/IMG_0598_2.JPEG",
                        "meta": "landscape 2048x1536 6x8",
                        "span": "right",
                        "crop": "outside landscape",
                        "caption": "top",
                    },
                    {
                        "photo": "mock-book/IMG_0598_2.JPEG",
                        "meta": "landscape 2048x1536 6x8",
                        "caption": "middle left",
                    },
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                        "meta": "portrait 1536x2048 6x8",
                        "span": "down",
                        "crop": "outside portrait",
                        "caption": "bottom right",
                    },
                    {
                        "photo": "mock-book/IMG_0598_2.JPEG",
                        "meta": "landscape 2048x1536 6x8",
                        "caption": "bottom left",
                    },
                ],
                "meta": "page 1 (right)",
                "caption": "page",
            },
        ]
    }

    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=book_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        render_mode = book_pdf.RenderMode(final=True)
        operations.do_render(
            console,
            [Path("mock-book.yml")],
            render_mode,
            out=out,
            launch=True,
            title="Test Book",
            spine="Test Book spine",
            unknown_yml=Mock(),
        )

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-book.yml")),
    ]

    assert mock_write_yaml.mock_calls == []

    assert mock_resampled_image.mock_calls == [
        call(
            "mock-book/IMG_0598_2.JPEG",
            CropDefinition(crop="outside landscape"),
            ImageRect(2287, 857),
            ImageBox(left=0, bottom=0, width=2287, height=857),
            page_colour=(255, 255, 255),
        ),
        call(
            "mock-book/IMG_0598_2.JPEG",
            CropDefinition(),
            ImageRect(1143, 857),
            ImageBox(left=0, bottom=0, width=1143, height=857),
            page_colour=(255, 255, 255),
        ),
        call(
            "mock-book/IMG_0598.JPEG",
            CropDefinition(crop="outside portrait"),
            ImageRect(1143, 1715),
            ImageBox(left=0, bottom=0, width=1143, height=1715),
            page_colour=(255, 255, 255),
        ),
        call(
            "mock-book/IMG_0598_2.JPEG",
            CropDefinition(),
            ImageRect(1143, 857),
            ImageBox(left=0, bottom=0, width=1143, height=857),
            page_colour=(255, 255, 255),
        ),
    ]
    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                left="1.587",
                bottom="18.6685",
                width="19.368",
                height="7.263",
                _data=[Caption.TopLeft, Caption.TopRight],
            ),
            mock_resampled_image.return_value,
        ),
        call(
            PdfBox(
                left="1.587",
                bottom="11.4055",
                width="9.684",
                height="7.263",
                _data=[Caption.MiddleLeft],
            ),
            mock_resampled_image.return_value,
        ),
        call(
            PdfBox(
                left="11.271",
                bottom="4.1425",
                width="9.684",
                height="14.526",
                _data=[Caption.MiddleRight, Caption.BottomRight],
            ),
            mock_resampled_image.return_value,
        ),
        call(
            PdfBox(
                left="1.587",
                bottom="4.1425",
                width="9.684",
                height="7.263",
                _data=[Caption.BottomLeft],
            ),
            mock_resampled_image.return_value,
        ),
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]


def test_do_render_error_page(mock_load_photo: Mock) -> None:
    book_: BookDict = {
        "pages": [
            {
                "page": [
                    {
                        "photo": "mock-book/PXL_20230803_073143580.jpg",
                    },
                    {
                        "photo": "mock-book/IMG_0598.JPEG",
                    },
                    {
                        "photo": "mock-book/RenderedImage.jpg",
                    },
                ],
            },
        ]
    }
    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=book_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        img1 = Mock()
        img2 = Mock()
        mock_resampled_image.side_effect = [
            img1,
            FileNotFoundError,
            img2,
        ]
        render_mode = book_pdf.RenderMode()
        operations.do_render(console, [Path("mock-book.yml")], render_mode, out=out)

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-book.yml")),
    ]

    assert mock_write_yaml.mock_calls == [call(Path("mock-book.yml"), book_)]

    assert mock_resampled_image.mock_calls == [
        call(
            "mock-book/PXL_20230803_073143580.jpg",
            CropDefinition("outside square"),
            ImageRect(762, 762),
            ImageBox(left=0, bottom=0, width=762, height=762),
            page_colour=(255, 255, 0),
        ),
        call(
            "mock-book/IMG_0598.JPEG",
            CropDefinition("outside square"),
            ImageRect(762, 762),
            ImageBox(left=0, bottom=0, width=762, height=762),
            page_colour=(255, 255, 0),
        ),
        call(
            "mock-book/RenderedImage.jpg",
            CropDefinition("outside square"),
            ImageRect(762, 762),
            ImageBox(left=0, bottom=0, width=762, height=762),
            page_colour=(255, 255, 0),
        ),
    ]
    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                left="1.587",
                bottom="18.265",
                width="6.456",
                height="6.456",
            ),
            img1,
        ),
        call(
            PdfBox(
                left="14.499",
                bottom="18.265",
                width="6.456",
                height="6.456",
            ),
            img2,
        ),
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]

    assert book_ == {
        "pages": [
            {
                "page": [
                    {"photo": "mock-book/PXL_20230803_073143580.jpg"},
                    {"photo": "mock-book/IMG_0598.JPEG"},
                    {"photo": "mock-book/RenderedImage.jpg"},
                ],
                "meta": "page 1 (right)",
            }
        ],
    }


def test_do_render_index() -> None:
    index_: BookDict = {
        "params": {"gutter": 0.5},
        "pages": [
            {
                "page": [
                    {
                        "caption": "Victoria",
                        "photo": "maps/vicTESTtoria-111x222_deadbeef.png",
                        "bounds": "(-34.09643350385901, 141.12511610945668) - (-39.08128136244812, 149.40833851937555)",
                        "lhs": [],
                        "rhs": [
                            {
                                "label": "*Not found*: inside front<br/>123 Home St: 1, inside back",
                                "y": 476,
                                "points": ["85be63cffffffff (606,726)"],
                            }
                        ],
                        "marker_radius": 0.1,
                    }
                ]
            },
            {
                "page": [
                    {
                        "caption": "August 2023",
                        "cal": {
                            1: "Tue",
                            2: "Wed",
                            3: "Thu inside front",
                            4: "Fri 1",
                            5: "Sat 2",
                            6: "Sun",
                            7: "Mon",
                            8: "Tue",
                            9: "Wed",
                            10: "Thu",
                            11: "Fri",
                            12: "Sat",
                            13: "Sun",
                            14: "Mon",
                            15: "Tue",
                            16: "Wed",
                            17: "Thu",
                            18: "Fri",
                            19: "Sat",
                            20: "Sun",
                            21: "Mon",
                            22: "Tue",
                            23: "Wed",
                            24: "Thu",
                            25: "Fri",
                            26: "Sat",
                            27: "Sun inside back",
                            28: "Mon",
                            29: "Tue",
                            30: "Wed",
                            31: "Thu",
                        },
                    }
                ]
            },
        ],
    }

    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=index_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(book_pdf.PhotoDetails, "load_photo") as mock_load_photo,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        mock_load_photo.return_value = book_pdf.PhotoDetails(
            filename="maps/vicTESTtoria-111x222_deadbeef.png",
            exif_datetime="",
            width=222,
            height=111,
            orientation="landscape",
            aspect="6x8",
        )
        render_mode = book_pdf.RenderMode()
        render_mode.annotate = False
        operations.do_render(console, [Path("mock-index.yml")], render_mode, out=out)

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-index.yml")),
    ]

    assert mock_write_yaml.mock_calls == []

    assert mock_resampled_image.mock_calls == []
    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                # left="5.899888888888888",
                # bottom="17.301166666666667",
                # width="10.742222222222223",
                # height="8.056666666666667",
                left=Fraction(53099, 9000),
                bottom=Fraction(103807, 6000),
                width=Fraction(2417, 225),
                height=Fraction(2417, 300),
            ),
            "maps/vicTESTtoria-111x222_deadbeef.png",
        ),
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]


def test_do_thumb_index() -> None:
    source_ = {
        "book_spine": "spine",
        "cols": 2,
        "rows": 2,
        "books": [{"source": ["book-1.yml"], "book": "book1"}],
    }
    book_1 = {"pages": [{"page": [{"photo": "photo-1.jpg"}]}]}
    index_ = {
        "book_title": "Untitled",
        "book_spine": "spine",
        "params": {"gutter": 0.5},
        "pages": [
            {
                "page": [
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                ],
                "layout": "g2x2",
            },
            {
                "page": [
                    {"photo": "BLANK", "thumb_title": "book1"},
                    {"photo": "BLANK"},
                    {
                        "photo": "photo-1.jpg",
                        "crop": "outside square top",
                        "thumb_page": "1",
                    },
                    {"photo": "BLANK"},
                ],
                "layout": "g2x2",
            },
        ],
    }
    with (
        patch.object(operations, "read_yaml", return_value=source_) as mock_read_yaml,
        patch.object(book_util, "read_yaml", return_value=book_1) as mock_read_yaml2,
        patch.object(operations, "write_yaml") as mock_write_yaml,
    ):
        operations.do_thumb_index(Path("mock-source.yml"))
    assert mock_read_yaml.mock_calls == [call(Path("mock-source.yml"))]
    assert mock_read_yaml2.mock_calls == [call(Path("book-1.yml"))]
    assert mock_write_yaml.mock_calls == [call(sys.stdout, index_)]


def test_do_render_thumb_index() -> None:
    index_: BookDict = {
        "book_title": "Untitled",
        "book_spine": "spine",
        "params": {"gutter": 0.5},
        "pages": [
            {
                "page": [
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                ],
                "layout": "g3x3",
            },
            {
                "page": [
                    {"photo": "BLANK", "thumb_subtitle": "book1a"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK"},
                    {"photo": "BLANK", "thumb_title": "book1"},
                    {"photo": "BLANK"},
                    {
                        "photo": "photo-1.jpg",
                        "crop": "outside square top",
                        "thumb_page": "1",
                    },
                    {"photo": "BLANK"},
                ],
                "layout": "g3x3",
            },
        ],
    }

    console = Mock()
    out = MagicMock()
    with (
        patch.object(
            book_util,
            "read_yaml",
            return_value=index_,
        ) as mock_read_yaml,
        patch.object(operations, "write_yaml") as mock_write_yaml,
        patch.object(book_pdf.PhotoDetails, "load_photo") as mock_load_photo,
        patch.object(photo_view, "resampled_image") as mock_resampled_image,
        patch.object(BookCanvas, "draw_photo") as mock_draw_photo,
    ):
        mock_load_photo.return_value = book_pdf.PhotoDetails(
            filename="photo-1.jpg",
            exif_datetime="",
            width=222,
            height=111,
            orientation="landscape",
            aspect="6x8",
        )
        render_mode = book_pdf.RenderMode()
        render_mode.annotate = False
        operations.do_render(console, [Path("mock-index.yml")], render_mode, out=out)

    assert mock_read_yaml.mock_calls == [
        call(Path("mock-index.yml")),
    ]

    assert mock_write_yaml.mock_calls == []

    assert mock_resampled_image.mock_calls == [
        call(
            "photo-1.jpg",
            CropDefinition("outside square top"),
            ImageRect(723, 723),
            ImageBox(left=0, bottom=0, width=723, height=723),
            page_colour=(255, 255, 0),
        )
    ]
    assert mock_draw_photo.mock_calls == [
        call(
            PdfBox(
                # left="14.197333333333333",
                # bottom="11.975666666666669",
                # width="6.1226666666666665",
                # height="6.1226666666666665",
                left=Fraction(5324, 375),
                bottom=Fraction(35927, 3000),
                width=Fraction(2296, 375),
                height=Fraction(2296, 375),
            ),
            mock_resampled_image.return_value,
        ),
    ]
    assert out.open.return_value.__enter__.return_value.mock_calls == [call.write(ANY)]
