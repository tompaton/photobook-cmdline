# User Guide


## Installation

1. Clone main repository

    ```bash
    git clone git@bitbucket.org:tompaton/photobook-cmdline.git
    cd photobook-cmdline
    ```

2. Create virtual environment

    ```bash
    hatch env create
    ```

3. Install requirements

    `jhead` and `jpegtran` are useful for ensuring photo files are correctly rotated.

    ```bash
    apt install jhead libjpeg-progs
    ```

    Installing reportlab requires pycairo which needs to be installed first

    ```bash
    apt install build-essential pkg-config python3-dev libcairo2-dev
    ```

4. Command line auto-completion

    Add this to `~/.bashrc`:

    ```bash
    eval "$(_PHOTOBOOK_COMPLETE=bash_source photobook)"
    ```


## Mapbox API key

A mapbox api key is required to generate map indexes.

https://docs.mapbox.com/help/getting-started/access-tokens/

This is loaded by `photobook` from a `photobook.toml` configuration file (in the
current or a parent directory), or from the `MAPBOX_KEY` environment variable.

```photobook.toml
MAPBOX_KEY="pk.abc123def456ghi789...xyz"
```

## Directory structure

```bash
/photobook
  /cmdline
    /src    # source code
    /schema # book schema definitions
  /maps     # cached map images
  /books    # book images, .yml files, generated .pdf files etc.
  photobook.toml  # configuration file
```

`/maps` needs to be there (or somewhere) so that it can be referenced 
in `map-preview.html`

## Checking the installation

```bash
hatch shell
photobook --help
```

## Editor configuration

Any text editor can be used to edit the book yaml files, but if possible you 
should use one that supports the yaml syntax and schema validation to assist by 
autocompleting values and catching syntax errors.

For example, Visual Studio Code using the `redhat.vscode-yaml` plugin.

The following schema definition can be included at the top of the book yaml file:

```yaml
# yaml-language-server: $schema=schema/book-schema.json
```

Note that the schema directory needs to be symlinked into the /books directory:

```bash
ln -sf ../schema books/schema
```

## Version control

Using git to version control the books/ directory is recommended as it provides
a good backup in case any edits go awry.

TODO: instructions to git init etc.

`.gitignore` file contents to exclude generated content:
```
*.pdf
*.generated
backups/
map-preview.html
output/
*-unknown-locations.yml
test-*
*-test*
schema
```

## Creating a photobook

1. Download photos (google, icloud)

2. Create and extract to `books/YYYY-mm` folder

3. Rotate photos

    ```bash
    jhead -autorot books/YYYY-mm/*
    ```

4. Create book in draft mode

   ```bash
   tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --maps books/map.yml books/YYYY-mm --launch
   ```

<!--
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --visualize --maps books/map.yml books/YYYY-mm | dot -Tsvg > docs/images/make-one-book.svg
-->
![Make subcommand visualization for one book](images/make-one-book.svg)

The first time `photobook make` is run for a directory you will be prompted for
the book title and whether you wish to generate captions using the dates photos
were taken. 

Note that at this point there are various "draft mode" features included in the 
PDF that won't be included in the final book:

- photo filenames
- large page numbers (easier to see when the PDF is zoomed out)
- yellow background to show cropped areas where photo doesn't match slot size
- pins to indicate geotagged location available from map.yml (green), and unknown 
  locations (orange)
- pages and cover are combined into a single PDF
- skipped photos are rendered in the margin of the page

To make a book without generating indexes, omit the `--maps` parameter:

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make books/YYYY-mm
```

<!--
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --visualize books/YYYY-mm | dot -Tsvg > docs/images/make-one-book-no-maps.svg
-->
![Make subcommand visualization for one book with no maps](images/make-one-book-no-maps.svg)


## Adding locations to `map.yml`

As the book is created, the `annotate` step will add locations for any geotagged
photos into the book yml.

The locations are added in the form of a `h3index`.
- https://h3geo.org/docs/api/indexing

Locations are looked up in the `map.yml` file based on the `h3index`.

Any unknown `h3index`s are written to a `book-unknown-locations.yml` file.

Each entry in this file includes a google map link and some relevant details to
help identify the location.

Once the location has been identified, it should be added to the `map.yml` file.

The contents of the `map.yml` file can be visualized in the `map-preview.html` file.

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook map-preview --maps books/map.yml  books/map-preview.html --launch
```

## Creating a combined volume

Proof version

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --maps books/map.yml books/YYYY-mm1 books/YYYY-mm2 books/YYYY-mm3 --launch
```

<!--
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --visualize --maps books/map.yml books/YYYY-mm1 books/YYYY-mm2 books/YYYY-mm3 | dot -Tsvg > docs/images/make-combined-volume.svg
-->

![Make subcommand visualization for one book](images/make-combined-volume.svg)

Final version (with separate pdfs for cover and pages)

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --maps books/map.yml --final books/YYYY-mm1 books/YYYY-mm2 books/YYYY-mm3
```

### Index position

Depending on the book layout, blank pages may be inserted before the map/calendar 
indexes in the combined `index.yml` so they are on the last pages of the book.

```yaml
- page: []
  layout: l1
  meta: blank page automatically inserted
```

These can be moved/removed if desired.

## Manually editing map and calendar indexes

The map index pages will include up to 3 maps per page.

The layout will be automatically selected between an "l3" layout (when there are
3 maps) and an "l2a" layout when there are 1 or 2 maps.

This can be overridden if desired by adding `layout: l3` to the map page in the 
index.

Labels can be edited, for example:
 - editing text/page numbers etc.
 - moving up/down, swaping sides
 - combining labels
 - hiding markers, leaders 
 - moving to a larger map
 - adding/removing insets

By omitting the `--maps` parameter `photobook make` will "watch" the index yaml 
file for changes and render (and optionally `--launch`).

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make books/YYYY-mm-index.yml --launch
```
![Make subcommand visualization for an index](images/make-custom-book-no-maps.svg)

### Example map yaml

<!-- docs/examples/map-index.yml -->
```yaml
params:
  gutter: 0.5
pages:
- page:
  - caption: Victoria
    photo: docs/examples/map-index-victoria.png
    bounds: (-34.09643350385901, 141.12511610945668) - (-39.08128136244812, 149.40833851937555)
    inset:
      - (-38.29236177807541, 143.56246948242185) - (-38.80306228724325, 144.4331359863281) Great Ocean Road
    lhs:
    - label: Default
      y: 700
      points:
      - 85be616bfffffff (606,726)
    - label: Points with noleader<br/>(label with line break)
      y: 400
      points:
      - 87be6a183ffffff hide
      - 8fbe6a734aa8d71 noleader
      - 8fbe6a1a18cd82d noleader
      - 88be6a0a57fffff
    rhs:
    - label: No marker
      y: 200
      points:
      - 8abe71660287fff nomarker

    - label: Multiple markers
      y: 500
      points:
      - 85be63cffffffff (606,726)
      - 85be600bfffffff
    meta: landscape 1268x952 6x8
    paths:
      example-path:
      - -36.6,145.7
      - -36.6,146.0
      - -36.5,146.1
      - -36.5,146.2
      - -36.4,146.2
      - -36.4,146.6
      - -36.5,146.6
      - -36.5,146.7
      - -36.6,146.7
      - -36.6,147.0
      - -36.5,147.0
      - -36.5,147.1
      - -36.7,147.1
      - -36.7,147.3
  meta: page 1 (right)
warning: blank front cover, inside front cover, inside back cover, back cover, last
  3 pages.
```

![Example map](images/map-index.svg)


## Using a manually generated book yaml file

You can also use the `make` command to render a book that isn't automatically 
generated from a directory of photos.  For example, if an automatically generated
file has been split into multiple files.

```bash
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --maps books/map.yml books/YYYY-mm-custom.yml
```

<!--
tom@sulfur:~/dev/photobook/photobook-cmdline$ photobook make --visualize --maps books/map.yml books/YYYY-mm-custom.yml | dot -Tsvg > docs/images/make-custom-book.svg
-->
![Make subcommand visualization for a custom book](images/make-custom-book.svg)


## Uploading to blurb

Checklist
- [ ] captions
- [ ] h3index
- [ ] aspect
- [ ] no blank pages
- [ ] facing pages
- [ ] map labels
- [ ] calendar captions

1. Login to blurb
2. Use the PDF to Book tool: https://au.blurb.com/pdf-to-book
3. Upload cover and pages pdfs
4. Choose Softcover, Matte 80gsm, Magazine 8.5x11
5. Update Title and Author if necessary
6. Upload and view proof
7. Check covers, spine, pages
8. Order book
9. Deselect digital book format
10. Place order


## Errors

If the photos on a page don't match an available layout (e.g. portrait-landscape-portrait) 
then an "error" page will be displayed showing the photos in a 3x3 square grid layout.

If the yaml syntax is invalid, an error will be displayed and you can fix the
file then hit enter (or Y) to try again or N to quit.
