# Programmers Guide

## Development

Repository/packages

Use `hatch` to install in virtual environment, run tests etc.

```bash
hatch env create
```

TODO: install requirements as per User Guide

Install non-python development dependencies:

```bash
sudo apt install graphviz
sudo apt-get install pdf2svg
```

Run tests, generete test coverage, etc.

```bash
hatch test
```

Linting, type checking:
```bash
hatch fmt
hatch run types:check
```

### Packages

Code is separated into separate packages.

Each is also managed by `hatch`.

1. `hatch version minor` or `hatch version patch` to bump version
2. commit changes
3. `git tag $(hatch version)`
4. `git push && git push origin $(hatch version)`

Update dependencies in `pyproject.toml`

## Generating documentation

Much of the documentation is intended to be automatically generated from the
code so there is a single source of truth.

* `Command Line Reference.md` is generated from the click --help
* `Photobook File Formats.md` is generated from the pydantic schemas

This includes generating some SVG diagrams in `User Guide.md`.

```bash
tom@sulfur:~/dev/photobook/cmdline$ hatch run make-docs
```

### Tests

The documentation generation is unit tested by comparing the generated values
against the documents committed (or staged for commit) in the repository.

This will ensure that the documentation is up to date (i.e. a code change
without also running `hatch run make-docs` will cause the tests to fail) and will
catch some changes in the code behaviour (e.g. if a layout or make visualize svg
image no longer matches.)

This permits a manual verification of any changes to the documentation contents
(i.e. run `hatch run make-docs` and `git add` to confirm the docs are correct),
and then automatic regression testing of the documentation via `pytest` from
that point on.

Comparing file changes visually
```bash
git show master:docs/examples/test-book.pdf > docs/examples/test-book~master.pdf
xdg-open docs/examples/test-book~master.pdf & disown
xdg-open docs/examples/test-book.pdf & disown
```

## Map marker layout

We want to compute positions of labels for markers on an image, such that the 
lines (leaders) between each marker and label (port) don't intersect.

Doing this optimally is complicated. e.g. https://arxiv.org/pdf/1702.01799.pdf

### Basic heuristic algorithm

However, there is a pretty good heuristic algorithm that results in good layouts
the majority of the time.

First, we assume all the labels are equally spaced at "ports" down the side (1, 2, 3, 4).
Then we need to select a marker (A, B, C, D) and connect it to each port with a line.

To do this for the port 1, we calculate the angles from port 2 to every marker.

We choose the minimum angle (A) and connect port 1 to marker A.

![Marker layout step 1](images/marker-layout-1.svg)

This partitions the map into a minimal region (yellow) containing only the first 
point and the line to it and the remaining region that contains all the other 
markers and ports so there can be no overlap.

Then we move on to the remaining markers, calculating the angles from port 3 to 
markers B, C and D.  The minimum angle (to B) therefore becomes the marker for port 2.

![Marker layout step 2](images/marker-layout-2.svg)

### Limitations

This needs a little modification because when measuring the angle to a marker we
need to adjust for the fact that the marker has a non-zero radius.

![Marker layout avoiding overlap](images/marker-layout-overlap.svg)

In the above image, choosing the smallest angle (to A) from the port 2 results 
in the red marker lines which intersect with the circle markers.

This can happen anywhere in a region along the line extending through port 2 and 
A.

![Regions where leader lines intersect](images/geogebra-export-basic-bad-regions.svg)

### Refinement 1: Measure angle using line tangent to the marker

So the first refinement is to measure the angle using lines tangent to the bottom of
the circles.

This ensures that the entire marker is within the region cut from the original map.


![Regions where leader lines intersect using tangents](images/geogebra-export-tangent-bad-regions.svg)

### Refinement 2: Offset port position

The partitioned region needs a little more padding as we want to avoid a marker 
circle extending outside the region where it might overlap the next line. 

When calculating the angles, we can offset the start position above the port by
the marker radius.

![Regions where leader lines intersect using offset tangents](images/geogebra-export-tangent-offset-bad-regions.svg)

This shrinks the area where there can be an overlap to a diamond shaped region
where B is in the "shadow" of A and can't see either port 1 or port 2.

Using shadertoy I was able to empirically compare the regions
https://www.shadertoy.com/view/mltyRH
and animate to see the behaviour over a variety of marker configurations.

![Alt text](images/shadertoy-marker-layout-overlap.png)

### TODO

TODO: explain step to adjust label positions based on text height

TODO: investigate iterating that step

TODO: explain step to partition labels between left and right sides

TODO: could probably run through a variety of percentiles to split the markers 
between left and right sides.

ISSUE: moving the mid-point to the center of the map (usually) results in a 
big imbalance between the number of markers on each side.  all markers on one
side has smaller total length, but labels may overlap.

TODO: choice of "best" layout needs to account for minimizing both the line 
length and any overlap between the labels. 

TODO: figure out if stepping through the ports from the top and bottom 
alternately still means we're guaranteed to never overlap the leaders.

## Photo resampling and cropping

photo crop parameters can be specified directly in the yml via `crop_x`, `crop_y`, `pan_x` and `pan_y` values.  these are resolution independent and specified as fractions of the image width/height.

`crop_x,crop_y` are the fraction of the original width/height (possibly larger than 1.0 if
the image doesn't reach the slot boundaries.)

`pan_x,pan_y` are the offsets of the image center. 0.0 is the middle.

more easily, they can be specified using `crop` in the "inside portrait left" format.

`SlotWindow.from_photo_model` calculates a rectangle relative to the image center from the pan/crop values.

`ImageSrc.from_slot_window` adds padding if necessary.

1. open image
2. add necessary padding around the image
  - create a larger image using the page background colour, or yellow when in preview mode
  - paste the photo in at the required offset
3. resample the image (to the proper dpi)
  - cut a rectangle out of the image (has to offset due to any added padding from step 2)
  - bicubic transform to the target width/height
  - source/dest aspect ratios can differ
4. crop the image to the final size
  - not quite sure when the clip rect is a different size to the image_rect? for covers?
5. save the image
  - this will be cached so all these steps are only run once for each image in each slot configuration


### Examples

<!-- generated by src/photobook/reference_test.py::test_photo_view_resample_image_svg -->

![test 1](images/resample_image-1.svg)

![test 2](images/resample_image-2.svg)

![test 3](images/resample_image-3.svg)

![test 4](images/resample_image-4.svg)

![test 7](images/resample_image-7.svg)

![test 5](images/resample_image-5.svg)

![test 6](images/resample_image-6.svg)

![test 8](images/resample_image-8.svg)

![test 9](images/resample_image-9.svg)


```bash
git show master:docs/images/resample_image-7.svg > docs/images/resample_image-7~master.svg
xdg-open docs/images/resample_image-7~master.svg & disown
xdg-open docs/images/resample_image-7.svg & disown
```

## Types

`photobook` uses a lot of rectangles (photos, pages, layouts, aspect ratios)

using these "dataclasses" reduces the number of parameters passed around, and 
reduces the number of names needed for local variables and parameters.

encapsulates the arithmetic operations to reduce errors.

use the type system to ensure that units of measurement are not confused.

pdf document uses centimeters. images use pixels.

also we want to use `Fraction`s when possible to avoid errors with floating 
point precision when calculating layouts.


### MarginDefinition

Defines the printable margin (in centimeters) on the inside (spine) and outside 
of the page.

Constructs to `Margin` instances.

### Margin

Defines an area as offsets (in centimeters) from the outsides.

### Rect, ImageRect

Defines an area (`width` and `height`).

`Rect` is in centimeters. `ImageRect` is in pixels.

### Box, PdfBox, ImageBox

Defines a rectangle at a particular position.

Position is `(left, top)` for `Box`, and `(left, bottom)` for `PdfBox` and `ImageBox`.

`ImageBox` is in pixels.  `Box` and `PdfBox` in centimeters.
