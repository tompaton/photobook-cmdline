<!-- AUTOMATICALLY GENERATED -- DO NOT EDIT -->
<!-- photobook reference schema > 'docs/Photobook File Formats.md' -->

# Photobook File Format

## Book schema

### Book format
```
format: "8.5x11M"
```
Format code to specify the book size

| Format code |    |
| --- | --- |
| `10x8L` | 10 x 8 Landscape (Blurb) |
| `8x10P` | 8 x 10 Portrait (Blurb) |
| `8.5x11M` | 8.5 x 11 Magazine (Blurb) |
| `7x7S` | 7 x 7 Square (Blurb) |

### Book parameters
```
params: BookParams
```
Default page configuration parameters for the book.

These will apply to all pages unless overridden by the page `params`.

### Book Title
```
book_title: ""
```

### Book Spine
```
book_spine: ""
```
Text to go on the spine of the book.

This will also be used as the PDF title.

### Front cover
```
front: PageModel | None
```
Front cover page contents

This page should be `fullscreen` and include a single photo.

A photo with a caption should have caption_position set to next.

### Inside front cover
```
in_front: PageModel | None
```
Inside front cover page contents

This can be any layout and is treated like a regular page, but not all book formats support printing the inside covers.

### Book pages
```
pages: [array of PageModel]
```
This is the list of book pages

### Inside back cover
```
in_back: PageModel | None
```
Inside back cover page contents

This can be any layout and is treated like a regular page, but not all book formats support printing the inside covers.

### Back cover
```
back: PageModel | None
```
Back cover page contents

This should contain a single photo that will be printed in landscape format above the book title.

A photo with a caption should have caption_position set to previous.

### Warning
```
warning: string | None
```

## Book params
### Page Mode
```
page_mode: "default"
```

### Gutter
```
gutter: 0 (number)
```

### Aspect
```
aspect: "6x8"
```

### Page Colour
```
page_colour: "white"
```

### Caption Position
```
caption_position: "bottom2"
```

### Full Page
```
full_page: False
```

### Scale
```
scale: 1.0 (number)
```

### Caption Columns
```
caption_columns: 1
```

### Caption Font Size
```
caption_font_size: 8
```

### Shift Page Number
```
shift_page_number: True
```

## Page schema

### Page Mode
```
page_mode: string | None
```

### Gutter
```
gutter: number | None
```

### Aspect ratio
```
aspect: string | None
```
Aspect ratio used by photos on the page.

Individual photos can use `crop: outside landscape` or `crop: outside portrait` to avoid being stretched into the page aspect ratio.

Availabe photo aspect ratios:

| Aspect code      | Description                              |
| ---------------- | ---------------------------------------- |
| `9x21            ` | 21 x 9 (cinema widescreen)               |
| `9x16            ` | 16 x 9 (cinema)                          |
| `Phi             ` | Golden Ratio                             |
| `4x6             ` | 4 x 6 (traditional print)                |
| `6x8             ` | 6 x 8 (digital)                          |
| `8x10            ` | 8 x 10 (print enlargement)               |
| `1x1             ` | Square                                   |

![Aspect ratios reference](images/reference-aspect.svg)

### Page Colour
```
page_colour: string | None
```

### Caption Position
```
caption_position: string | None
```

### Full Page
```
full_page: False
```

### Scale
```
scale: 1.0 (number)
```

### Caption Columns
```
caption_columns: 1
```

### Caption Font Size
```
caption_font_size: integer | None
```

### Shift Page Number
```
shift_page_number: boolean | None
```

### Meta
```
meta: string | None
```

### Note
```
note: string | None
```

### Page layout
```
layout: string | None
```
Layout code for the page.

Usually this is unnecessary as it will be inferred from the selected photos.

Available page layouts:

| Layout code      | Photo orientations                       |     |
| ---------------- | ---------------------------------------- | --- |
| l1               | landscape                                | ![l1 thumbnail](images/reference-layout-l1.svg) |
| p1               | portrait                                 | ![p1 thumbnail](images/reference-layout-p1.svg) |
| p1a              | portrait                                 | ![p1a thumbnail](images/reference-layout-p1a.svg) |
| s1               | square                                   | ![s1 thumbnail](images/reference-layout-s1.svg) |
| l2               | landscape, landscape                     | ![l2 thumbnail](images/reference-layout-l2.svg) |
| p2               | portrait, portrait                       | ![p2 thumbnail](images/reference-layout-p2.svg) |
| l2a              | landscape, landscape                     | ![l2a thumbnail](images/reference-layout-l2a.svg) |
| s2a              | square, square                           | ![s2a thumbnail](images/reference-layout-s2a.svg) |
| s2b              | square, square                           | ![s2b thumbnail](images/reference-layout-s2b.svg) |
| pll              | P, L, L                                  | ![pll thumbnail](images/reference-layout-pll.svg) |
| lpp              | L, P, P                                  | ![lpp thumbnail](images/reference-layout-lpp.svg) |
| llp              | L, L, P                                  | ![llp thumbnail](images/reference-layout-llp.svg) |
| ppl              | P, P, L                                  | ![ppl thumbnail](images/reference-layout-ppl.svg) |
| pss              | P, S, S                                  | ![pss thumbnail](images/reference-layout-pss.svg) |
| lss              | L, S, S                                  | ![lss thumbnail](images/reference-layout-lss.svg) |
| ssp              | S, S, P                                  | ![ssp thumbnail](images/reference-layout-ssp.svg) |
| ssl              | S, S, L                                  | ![ssl thumbnail](images/reference-layout-ssl.svg) |
| l3               | L, L, L                                  | ![l3 thumbnail](images/reference-layout-l3.svg) |
| p3               | P, P, P                                  | ![p3 thumbnail](images/reference-layout-p3.svg) |
| l4               | L, L, L, L                               | ![l4 thumbnail](images/reference-layout-l4.svg) |
| s4               | S, S, S, S                               | ![s4 thumbnail](images/reference-layout-s4.svg) |
| p4               | P, P, P, P                               | ![p4 thumbnail](images/reference-layout-p4.svg) |
| lplp             | L, P, L, P                               | ![lplp thumbnail](images/reference-layout-lplp.svg) |
| ppll             | P, P, L, L                               | ![ppll thumbnail](images/reference-layout-ppll.svg) |
| plpl             | P, L, P, L                               | ![plpl thumbnail](images/reference-layout-plpl.svg) |
| llpp             | L, L, P, P                               | ![llpp thumbnail](images/reference-layout-llpp.svg) |
| lsls             | L, S, L, S                               | ![lsls thumbnail](images/reference-layout-lsls.svg) |
| ppss             | P, P, S, S                               | ![ppss thumbnail](images/reference-layout-ppss.svg) |
| slsl             | S, L, S, L                               | ![slsl thumbnail](images/reference-layout-slsl.svg) |
| sspp             | S, S, P, P                               | ![sspp thumbnail](images/reference-layout-sspp.svg) |
| lppl             | L, P, P, L                               | ![lppl thumbnail](images/reference-layout-lppl.svg) |
| pllp             | P, L, L, P                               | ![pllp thumbnail](images/reference-layout-pllp.svg) |
| l3p2             | L, L, L, P, P                            | ![l3p2 thumbnail](images/reference-layout-l3p2.svg) |
| p3l2             | P, P, P, L, L                            | ![p3l2 thumbnail](images/reference-layout-p3l2.svg) |
| p2l3             | P, P, L, L, L                            | ![p2l3 thumbnail](images/reference-layout-p2l3.svg) |
| l2p3             | L, L, P, P, P                            | ![l2p3 thumbnail](images/reference-layout-l2p3.svg) |
| l3s2             | L, L, L, S, S                            | ![l3s2 thumbnail](images/reference-layout-l3s2.svg) |
| p3s2             | P, P, P, S, S                            | ![p3s2 thumbnail](images/reference-layout-p3s2.svg) |
| s2l3             | S, S, L, L, L                            | ![s2l3 thumbnail](images/reference-layout-s2l3.svg) |
| s2p3             | S, S, P, P, P                            | ![s2p3 thumbnail](images/reference-layout-s2p3.svg) |
| l6               | L, L, L, L, L, L                         | ![l6 thumbnail](images/reference-layout-l6.svg) |
| p6               | P, P, P, P, P, P                         | ![p6 thumbnail](images/reference-layout-p6.svg) |
| p6b              | P, P, P, P, P, P                         | ![p6b thumbnail](images/reference-layout-p6b.svg) |
| l3p3a            | L, P, L, P, L, P                         | ![l3p3a thumbnail](images/reference-layout-l3p3a.svg) |
| l3p3c            | L, P, P, L, L, P                         | ![l3p3c thumbnail](images/reference-layout-l3p3c.svg) |
| l3p3b            | P, L, P, L, P, L                         | ![l3p3b thumbnail](images/reference-layout-l3p3b.svg) |
| l3p3d            | P, L, L, P, P, L                         | ![l3p3d thumbnail](images/reference-layout-l3p3d.svg) |
| p3l3a            | P, P, P, L, L, L                         | ![p3l3a thumbnail](images/reference-layout-p3l3a.svg) |
| p3l3c            | P, L, P, L, P, L                         | ![p3l3c thumbnail](images/reference-layout-p3l3c.svg) |
| p3l3b            | L, L, L, P, P, P                         | ![p3l3b thumbnail](images/reference-layout-p3l3b.svg) |
| p3l3d            | L, P, L, P, L, P                         | ![p3l3d thumbnail](images/reference-layout-p3l3d.svg) |
| p3x3             | portrait (9)                             | ![p3x3 thumbnail](images/reference-layout-p3x3.svg) |
| p3x4             | portrait (12)                            | ![p3x4 thumbnail](images/reference-layout-p3x4.svg) |
| p4x4             | portrait (16)                            | ![p4x4 thumbnail](images/reference-layout-p4x4.svg) |
| l3x3             | landscape (9)                            | ![l3x3 thumbnail](images/reference-layout-l3x3.svg) |
| l3x4             | landscape (12)                           | ![l3x4 thumbnail](images/reference-layout-l3x4.svg) |
| l3x5             | landscape (15)                           | ![l3x5 thumbnail](images/reference-layout-l3x5.svg) |
| l4x4             | landscape (16)                           | ![l4x4 thumbnail](images/reference-layout-l4x4.svg) |
| g3x3             | square (9)                               | ![g3x3 thumbnail](images/reference-layout-g3x3.svg) |
| g7x10            | square (70)                              | ![g7x10 thumbnail](images/reference-layout-g7x10.svg) |

NOTE: quite a few of the above layouts are not centered/bounded correctly within the page.That isn't a problem in practice because the bounding box is used, not the absolute positions.

### Page
```
page: [array of PhotoModel | CalendarModel | MapContentModel | ThumbPhotoModel]
```

### Caption
```
caption: ""
```

### Calendar index caption
```
cal_caption: string | None
```
Provides an optional additional description for the page entry in the calendar index.

Use `cal_caption: null` to suppress a calendar index for the page (e.g. for related photos that were not taken on the date of the event or where the date is irrelevant or incorrect).

Captions for a single date appearing on multiple pages will be concatenated together with commas (after removing any duplicates).

## Photo schema

### Photo
```
photo: "BLANK"
```

### Photo caption
```
caption: ""
```
Optional caption describing the photo, to be displayed at the bottom/side of the page

### Crop
```
crop: ""
```

### Pan X
```
pan_x: 0.0 (number)
```

### Pan Y
```
pan_y: 0.0 (number)
```

### Crop X
```
crop_x: 1.0 (number)
```

### Crop Y
```
crop_y: 1.0 (number)
```

### Extend photo slot to edge of page
```
extend: ""
```
Extend the photo slot to the edge of the page.

Can be any of `left`, `right`, `top`, `bottom`.

### Meta
```
meta: string | None
```

### Skip photo
```
skip: False
```
Exclude the photo from the page layout.  A thumbnail of the photo will be included at the side of the page for reference, this will not be included in the final output.

### Span photo
```
span: ""
```
Allow the photo to span across adjacent slots.

Can be `[n] (right|down)`, if `n` is omitted a single extra slot is spanned.

### Note
```
note: string | None
```
"Sticky note" displayed on photo when editing, this will not be included in the final output.

### H3Index
```
h3index: string | None
```

### Calendar index caption
```
cal_caption: string | None
```
Provides an optional additional description for the page containing this photo's entry in the calendar index.

See Page : cal_caption for more details.

### Photo timestamp
```
timestamp: string | None
```
Timestamp from photo EXIF data.

Will be filled in by `annotate --dates` command.

## Map Index schema

### Map filename
```
photo: "BLANK"
```
Filename of map image.  For backwards compatibility `photo` is also supported.

### Map caption
```
caption: ""
```
For reference only, not displayed

### Note
```
note: ""
```
"Sticky note" displayed on map when editing, will not be included in the final output

### Map bounds
```
bounds: string | None
```
Latitude and longitude coordinates of top-left and bottom-right map boundary.

Format is `(top_lat, left_lon) - (bottom_lat, right_lon)`

Decimal degrees, S/W as negative values.

### Marker Radius
```
marker_radius: 0.1 (number)
```

### Left side map labels
```
lhs: [array of LabelModel]
```
List of labels to be displayed on the left-hand-side of the map.

### Right side map labels
```
rhs: [array of LabelModel]
```
List of labels to be displayed on the right-hand-side of the map.

### Map inset boxes
```
inset: [array of string]
```
List of inset boxes to be rendered on the map to indicate the positions of other maps that will include more detail.

Format is `(top_lat, left_lon) - (bottom_lat, right_lon)` followed by an optional description/comment

Decimal degrees, S/W as negative values.

### Paths to render on the map
```
paths: {} (object)
```
Each path is a list of decimal `lat,lon` strings

## Label schema
### Label text
```
label: ""
```
Label text.  Use html markup and entities for formatting.

### Map marker points
```
points: [array of string]
```
List of marker points connected to this label.

Each label is a string in the format `h3index [mode] (x,y)`

h3index is the coordinate of the marker.

(x,y) are the pixel coordinates (for reference only, only the h3index is used).

mode is optional and one of:
- `hide` - don't show this marker
- `nomarker` - show only the line between the label and marker
- `noleader` - show only the circle marker with no line

### Label y-coordinate
```
y: 0
```
Y-coordinate (in pixels from the top) of the marker label.

## Calendar Index schema

### Calendar caption
```
caption: ""
```
Caption titling the calendar, usually the month/year

### Calendar entries
```
cal: {} (object)
```
The calendar is a dictionary of day numbers mapping to the label to be displayed for the day.

Use html markup and entities for formatting label text.

### Note
```
note: ""
```
"Sticky note" displayed on calendar when editing, will not be included in the final output

# Thumbnail Index File Format

### Photo filename
```
photo: "BLANK"
```
Filename of thumbnail image.

### Crop mode
```
crop: "outside square top"
```
Crop mode for the thumbnail photo.

### Page number
```
thumb_page: ""
```
Page number displayed on thumbnail photo.

### Title
```
thumb_title: ""
```
Book title displayed in thumbnail page. Should be on it's own row and the photo should be BLANK.

### Subtitle
```
thumb_subtitle: ""
```
Book subtitle displayed in thumbnail page. Should be on it's own row and the photo should be BLANK.

# Map File Format

### Maps
```
maps: [array of MapModel]
```
List of maps that markers will be placed on to generate the map indexes.

### Locations
```
locations: [array of LocationModel]
```
List of locations that photos will be matched against to generate the map indexes.

## Map schema


Example:

```yaml
maps:
  - map: melbourne
    title: Melbourne
    center: -37.84 144.93
    zoom: 9.50
    h3resolution: 8
```

### Map slug
```
map: ""
```
Short, unique slug for filenames for cached maps

### Map title
```
title: ""
```
Label/title for map

### Map center
```
center: ""
```
Latitude and longitude coordinates of the map center. Decimal, separated by a space. S/W as negative values.

### Map zoom factor
```
zoom: None (number)
```
Mapbox zoom factor for the map

### H3 grid resolution
```
h3resolution: None
```
H3 resolution for marker positions on the map

## Location schema


Example:

```yaml
locations:
- location: Melbourne Zoo
    cal_index: true
    hexes:
    - 87be63540ffffff,7
    - 8fbe635454ccca9,7
    - 8fbe63541db52a1,7
```


### Location name
```
location: ""
```
Location name/description that will be used in the map index marker labels

### Location hexes
```
hexes: [array of string]
```
The hexes are a list of H3 hex map indexes that make up the location, followed by the "zoom level" for the location to indicate the size of the hex.

Specifying the zoom explicitly makes it easy to take a photo h3index and use it as the "center" point of the hex, and change the zoom later if necessary. e.g. `8fbe63ccb2c181b,8`

### Use a single map marker for location
```
collect: True
```
When `collect: true` all the photos in hexes in that location will be positioned on the map with a single pointer (to the first hex).

When `collect: false` each photo will be positioned where it was taken. e.g. this is useful for large hexes or walking tracks etc.

### Automatically annotate location in calendar index
```
cal_index: False
```
When `cal_index: true`, the location name will be automatically added into the `cal_caption` for the photo.

This can be used for significant locations (holiday destinations etc.) to speed up creation and reduce manual editing of the index yaml file.

### Link to google map of the location
```
map_url: string | None
```
This will be automatically generated for unknown locations when annotating.

### Number of photos at the location
```
count: 1
```
This will be automatically filled in for unknown locations when annotating.

### Date of first photo at the location
```
first_date: ""
```
This will be automatically filled in for unknown locations when annotating.

### Date of last photo at the location
```
last_date: ""
```
This will be automatically filled in for unknown locations when annotating.

### Adjacent locations
```
nearby: ""
```
This will be automatically filled in for unknown locations when annotating.


