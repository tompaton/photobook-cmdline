<!-- AUTOMATICALLY GENERATED -- DO NOT EDIT -->
<!-- photobook reference cli > 'docs/Command Line Reference.md' -->

# Command Line Reference

Primary commands:
  - `photobook make`

Sub-commands for specific steps:
  - `photobook generate book`
  - `photobook annotate`
  - `photobook generate index`
  - `photobook generate thumb-index`
  - `photobook render`
  - `photobook map-preview`

Reference commands for self-documentation:
  - `photobook reference aspects`
  - `photobook reference cli`
  - `photobook reference layouts`
  - `photobook reference schema`
  - `photobook make --visualize`


## photobook

Photobook creation tool

### Usage

```
photobook [OPTIONS] COMMAND [ARGS]...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## photobook generate


Commands for generating elements of the book


### Usage

```
photobook generate [OPTIONS] COMMAND [ARGS]...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## generate thumb-index


Generate photobook thumbnail index

Generate photobook thumbnail index from YAML description
and print to stdout.

INPUT_YAML is the thumbnail index input filename.


### Usage

```
photobook generate thumb-index [OPTIONS] INPUT_YAML
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## generate book


Generate book yaml

Generate book yaml for all the photos in the given directory and write to
BOOK_FILE.


### Usage

```
photobook generate book [OPTIONS] PHOTO_PATH BOOK_FILE
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## generate index


Generate photobook map/calendar index

Generate photobook map index from YAML descriptions of books
and write to an index file.

BOOK_FILE is the input filename, if multiple files are provided the books
will be concatenated into a single volume.


### Usage

```
photobook generate index [OPTIONS] BOOK_FILE...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--maps` | FILE  (REQUIRED) | yaml database of maps and locations |
| `--out  -o` | FILE  (REQUIRED) | index output file |
| `--insert-blanks` |   | Insert blank pages before index |
| `--help` |   | Show this message and exit. |



## photobook reference


Reference commands for generating self-documentation


### Usage

```
photobook reference [OPTIONS] COMMAND [ARGS]...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## reference aspects

Output available photo aspect ratios.

### Usage

```
photobook reference aspects [OPTIONS]
```

| Options |      |      |
| ------- | ---- | ---- |
| `--table` |   | output markdown table of aspect ratios to stdout |
| `--svg` |   | output svg diagram of aspect ratios to stdout |
| `--help` |   | Show this message and exit. |



## reference layouts

Output available page layouts.

### Usage

```
photobook reference layouts [OPTIONS]
```

| Options |      |      |
| ------- | ---- | ---- |
| `--table` |   | output markdown table of page layouts to stdout |
| `--svg` |   | output svg diagrams of page layouts to docs/images/reference-layout-xxx.svg |
| `--help` |   | Show this message and exit. |



## reference cli

Generate reference documentation for commands and options.

### Usage

```
photobook reference cli [OPTIONS]
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## reference schema

Generate reference documentation for YAML schemas.

### Usage

```
photobook reference schema [OPTIONS]
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## photobook config


Configuration commands


### Usage

```
photobook config [OPTIONS] COMMAND [ARGS]...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## config show


Show photobook configuration


### Usage

```
photobook config show [OPTIONS]
```

| Options |      |      |
| ------- | ---- | ---- |
| `--help` |   | Show this message and exit. |



## photobook render


Render photobook from YAML description

BOOK_FILE is the input filename, if multiple files are provided the books
will be concatenated into a single pdf.


### Usage

```
photobook render [OPTIONS] BOOK_FILE...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--out  -o` | FILE  | defaults to BOOK_FILE with .pdf suffix |
| `--no-cover` |   | don't render cover pages |
| `--no-pages` |   | don't render inside pages |
| `--no-annotate` |   | don't annotate the input yaml with page numbers etc. |
| `--debug` |   | render bounding boxes etc. for debugging |
| `--title` | TEXT  | override book title |
| `--spine` | TEXT  | override book spine |
| `--launch` |   | open the generated pdf in viewer |
| `--final` |   | render for final output |
| `--help` |   | Show this message and exit. |



## photobook annotate


Annotate book with details from photos

Annotate BOOK_FILE with datetimes and/or h3indexes by looking up photo
locations.


### Usage

```
photobook annotate [OPTIONS] BOOK_FILE...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--maps` | FILE  | Annotate h3indexes by looking up photo locations in yaml database of maps and locations |
| `--dates` |   | Annotate with datetimes from photo EXIF data |
| `--help` |   | Show this message and exit. |



## photobook map-preview


Generate map html preview

Generate map html preview and write to MAP_HTML.


### Usage

```
photobook map-preview [OPTIONS] MAP_HTML
```

| Options |      |      |
| ------- | ---- | ---- |
| `--maps` | FILE  (REQUIRED) | yaml database of maps and locations |
| `--launch` |   | open the map html preview in browser |
| `--help` |   | Show this message and exit. |



## photobook make


Generate and render book

Check for changes and generate/render book.


### Usage

```
photobook make [OPTIONS] PHOTO_PATH...
```

| Options |      |      |
| ------- | ---- | ---- |
| `--maps` | FILE  | yaml database of maps and locations |
| `--final` |   | render pages and cover as separate files for final output |
| `--debug` |   | render bounding boxes etc. for debugging |
| `--visualize` |   | Output a graphviz (.dot) graph visualizing the dependencies between the input and output files instead of executing |
| `--launch` |   | open the generated files in browser/pdf viewer |
| `--no-meta` |   | Don't add meta/warnings to input yml file |
| `--help` |   | Show this message and exit. |


