# Photobook generator

This is a command line utility to generate photobooks.

[![PyPI - Version](https://img.shields.io/pypi/v/cmdline.svg)](https://pypi.org/project/cmdline)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/cmdline.svg)](https://pypi.org/project/cmdline)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install cmdline
```

## License

`cmdline` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.

## Workflow

    ,---------------------.     ,----------------.     ,---------------.
    | Directory of photos | ==> | YAML text file | ==> | Photobook PDF |
    '---------------------'     '----------------'     '---------------'

Example yaml

```yaml
book_title: January 2023
book_spine: January 2023
params:
  gutter: 0.5
front:
  layout: p1
  full_page: true
  page:
  - photo: 2023-01/IMG_0002.JPEG
    meta: portrait 1536x2048 6x8
    h3index: 8fbe616b280b943 "Sandy Gully beach"
in_front:
  page:
  - photo: 2023-01/IMG_9895.JPG
    meta: Left, portrait 828x1104 6x8
  - photo: 2023-01/IMG_9896.JPG
    meta: Right, portrait 828x1104 6x8
  caption: Inverloch
pages:
- page:
  - photo: 2023-01/20230102_110227.jpg
    meta: Top, landscape 4160x3120 6x8
    h3index: 8fbe600926e64e1 "Inverloch"
    crop: landscape outside
  caption: Inverloch
  aspect: 9x16
  meta: page 1 (right)
- page:
  - photo: 2023-01/IMG_9898.JPEG
    meta: landscape 2048x1536 6x8
    h3index: 8fbe63cdcab119e "Watson Park"
  caption: Watson Park
  meta: page 2 (left)
```

Example book

![Full page cover photo](docs/images/mockup-cover.png)
![Title on back cover](docs/images/mockup-back.png)
![Sample page](docs/images/sample-page.png)
![Sample index pages](docs/images/sample-index.png)

Commandline screenshot

![Make photobook](docs/images/screenshot-make.png)


## Features

The photobook layout is opinionated and page layouts are chosen based on the
photo orientations to maximize photo area and create a clean uniform aesthetic.

This generates a high quality book with very little manual intervention.

* Automatic page layout
* Supports multiple book formats
* Photo and page captions
* Indexes based on photo geotags and timestamps

## Documentation

[User Guide](docs/User%20Guide.md)

[Command Line Reference](docs/Command%20Line%20Reference.md)

[Photobook File Formats](docs/Photobook%20File%20Formats.md)

[Programmers Guide](docs/Programmers%20Guide.md)

## Contact

tom.paton@gmail.com

Project link: https://bitbucket.org/tompaton/photobook-cmdline/src/master/ 


